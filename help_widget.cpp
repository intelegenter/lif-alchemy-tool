#include "help_widget.h"
#include "ui_help_widget.h"

#include <QFile>

HelpWidget::HelpWidget(Enums::lang language, QWidget *parent) :
  QWidget(parent),
  ui(new Ui::HelpWidget)
{
  ui->setupUi(this);
  QString filePath;
  switch(language)
  {
  case Enums::eng:
    filePath = ":/resources/docs/Help_en.html";
    break;
  case Enums::ru:
    filePath = ":/resources/docs/Help_ru.html";
    break;
  }

  QFile help(filePath);
  if(help.open(QIODevice::ReadOnly))
    ui->textEdit->setHtml(help.readAll());

}

HelpWidget::~HelpWidget()
{
  delete ui;
}
