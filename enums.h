#ifndef ENUMS_H
#define ENUMS_H
#include <QObject>

using BCSet = QSet<int>;

namespace Enums
{
Q_NAMESPACE

enum lang
{
  eng = 0,
  ru = 1
};

enum HerbRoles
{
  iconRole = Qt::UserRole + 1,
  nameRole,
  propertiesRole,
  badCombinationsRole,
  availableActionsRole,
  itemStateRole,
  herbEnumValueRole,

  lastHerbRole
};
enum WorkMode
{
  manualMode,
  excMode,
  combMode,
  excCombMode
};
enum ItemState
{
  isNone = 0,
  isGood,
  isBad,
  isSelect,
  isExcMode,
  isExcCombMode,
  isCombMode
};
Q_ENUM_NS(ItemState)

enum Action
{
  noActions = 0b1,
  enterExcModeAction = noActions << 1,
  enterCombModeAction = enterExcModeAction << 1,
  enterExcCombModeAction = enterCombModeAction << 1,
  goodCombAction = enterExcCombModeAction << 1,
  badCombAction = goodCombAction << 1,
  selectAction = badCombAction << 1,
  badAndSetAction = selectAction << 1
};
Q_DECLARE_FLAGS(Actions, Action)
Q_FLAG_NS(Action)
}
#endif // ENUMS_H
