#ifndef HERBPROXYMODEL_H
#define HERBPROXYMODEL_H

#include <QSortFilterProxyModel>
#include "enums.h"
#include "propset.h"

class HerbsDataModel;
class HerbProxyModel : public QSortFilterProxyModel
{
  Q_OBJECT
public:
  explicit HerbProxyModel(HerbsDataModel* herbsModel, QObject *parent = nullptr);
  void setManualFilters(Props::Set pand = Props::Set(),
                        Props::Set por = Props::Set(),
                        Props::Set pstrictOr = Props::Set(),
                        Props::Set pnot = Props::Set());
  Props::Set andFilter() const;
  Props::Set orFilter() const;
  Props::Set strictOrFilter() const;
  Props::Set notFilter() const;

signals:
  void highlightedPropSetChanged(Props::Set props);

private:
  HerbsDataModel* mHerbsModel;
  Props::Set mAndFilter;
  Props::Set mOrFilter;
  bool mEmptyOrNotPass = false;
  Props::Set mStrictOrFilter;
  Props::Set mNotFilter;
  QHash<QModelIndex, Props::Set> mGoodMap;
  QHash<QModelIndex, Props::Set> mBadMap;
  QHash<QModelIndex, Props::Set> mSelectMap;

private:
  void onModeChanged(Enums::WorkMode mode);
  void onModelDataChanged(const QModelIndex &topLeft, const QModelIndex &, const QList<int> &roles = QList<int>());
  void onModelReset();
  void onModelAboutToReset();

  void rescanFilters();

protected:
  bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;
};

#endif // HERBPROXYMODEL_H
