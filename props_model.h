#ifndef PROPSMODEL_H
#define PROPSMODEL_H

#include <QStandardItemModel>
#include <herbs.h>
#include "propset.h"

class PropsModel : public QStandardItemModel
{
  Q_OBJECT
  public:
  explicit PropsModel(QObject *parent = nullptr);
    // QAbstractItemModel interface
  public:
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;

    Props::Set getPropSet(QStandardItem* item);
};

#endif // PROPSMODEL_H
