<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en" sourcelanguage="en_US">
<context>
    <name>Constants</name>
    <message>
        <location filename="herbs.h" line="134"/>
        <source>Flux</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="135"/>
        <source>Naphta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="136"/>
        <source>Food Flavour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="137"/>
        <source>Lowers max sHP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="138"/>
        <source>Damage sHP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="139"/>
        <source>RestoresHP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="140"/>
        <source>Damage hHP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="141"/>
        <source>Antidote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="142"/>
        <source>Damage hStam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="143"/>
        <source>Restore hStam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="144"/>
        <source>Damage sStam regen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="145"/>
        <source>Lowers max sStam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="146"/>
        <source>Raise max sStam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="147"/>
        <source>Raise Con</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="148"/>
        <source>Raise Will</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="149"/>
        <source>Raise Int</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="150"/>
        <source>Raise Str</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="151"/>
        <source>Raise Agi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="197"/>
        <source>Acerba Moretum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="198"/>
        <source>Adipem Nebulo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="199"/>
        <source>Albus Viduae</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="200"/>
        <source>Aquila Peccatum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="201"/>
        <source>Aureus Magistrum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="202"/>
        <source>Bacce Hamsa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="203"/>
        <source>Burmenta Wallo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="204"/>
        <source>Caeci Custos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="205"/>
        <source>Chorea Iram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="206"/>
        <source>Curaila Jangha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="207"/>
        <source>Curva Manus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="208"/>
        <source>Desertus Smilax</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="209"/>
        <source>Dulcis Radix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="210"/>
        <source>Dustali Krabo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="211"/>
        <source>Errantia Ludaeo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="212"/>
        <source>Fakha Rudob</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="213"/>
        <source>Falcem Malleorum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="214"/>
        <source>Fassari Tolge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="215"/>
        <source>Filia Prati</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="216"/>
        <source>Fohatta Torn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="217"/>
        <source>Fuskegtra Xelay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="218"/>
        <source>Gortaka Messen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="219"/>
        <source>Gratias Sivara</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="220"/>
        <source>Hallatra Kronye</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="221"/>
        <source>Holmatu Stazo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="222"/>
        <source>Huryosa Gulla</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="223"/>
        <source>Ital Iranta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="224"/>
        <source>Jenaro Vannakam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="225"/>
        <source>Jukola Beshaar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="226"/>
        <source>Kacaro Vilko</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="227"/>
        <source>Kaleda Mesgano</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="228"/>
        <source>Kalya Nori</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="229"/>
        <source>Khalari Gratsi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="230"/>
        <source>Kromenta Salicia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="231"/>
        <source>Kurupa Andhere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="232"/>
        <source>Kyasaga Sherl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="233"/>
        <source>Laster Kutta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="234"/>
        <source>Lobos Gamsa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="235"/>
        <source>Mala Fugam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="236"/>
        <source>Mauna Boba</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="237"/>
        <source>Memen Anik</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="238"/>
        <source>Mons Bastardus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="239"/>
        <source>Muncha Vana</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="240"/>
        <source>Murkha Bola</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="241"/>
        <source>Naraen Pandanomo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="242"/>
        <source>Nequissimum Propodium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="243"/>
        <source>Nocte Lumen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="244"/>
        <source>Oscularetur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="245"/>
        <source>Pecuarius Ventus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="246"/>
        <source>Persetu Hara</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="247"/>
        <source>Petra Stellam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="248"/>
        <source>Phlavar Pharest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="249"/>
        <source>Pitaku Koro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="250"/>
        <source>Pungentibus Chorea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="251"/>
        <source>Rakta Stema</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="252"/>
        <source>Remerta Poskot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="253"/>
        <source>Ripyote Quamisy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="254"/>
        <source>Rosa Kingsa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="255"/>
        <source>Saltare Diabolus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="256"/>
        <source>Sapienta Mantis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="257"/>
        <source>Sarmento Gaute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="258"/>
        <source>Suryodaya Bhagya</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="259"/>
        <source>Topasa Maidana</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="260"/>
        <source>Uliya Sundara</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="261"/>
        <source>Utrokka Khuru</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="262"/>
        <source>Vertato Zonda</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="263"/>
        <source>Viridi Ursae</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="264"/>
        <source>Aktashite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="265"/>
        <source>Bixbyite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="266"/>
        <source>Chrysoberyl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="267"/>
        <source>Euclase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="268"/>
        <source>Kutnohorite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="269"/>
        <source>Meionite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="270"/>
        <source>Scolecite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="271"/>
        <source>Gahnite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="272"/>
        <source>Tellurite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="273"/>
        <source>Urea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="274"/>
        <source>Humite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="275"/>
        <source>Parsonsite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="276"/>
        <source>Animal calculi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="277"/>
        <source>Blinded eye</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="278"/>
        <source>Bone meal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="279"/>
        <source>Crystalized bile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="280"/>
        <source>Digested feather</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="281"/>
        <source>Glowing urine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="282"/>
        <source>Odd claw</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="283"/>
        <source>Overgrown parasite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="284"/>
        <source>Piece of diseased hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="285"/>
        <source>Purbon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="286"/>
        <source>Strange muscle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs.h" line="287"/>
        <source>Uncommon tendon</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FiltersDialog</name>
    <message>
        <location filename="filtersdialog.ui" line="14"/>
        <source>Select properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="filtersdialog.cpp" line="18"/>
        <source>Pick filters from lists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="filtersdialog.cpp" line="40"/>
        <source>AND</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="filtersdialog.cpp" line="43"/>
        <source>OR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="filtersdialog.cpp" line="46"/>
        <source>STRICT OR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="filtersdialog.cpp" line="49"/>
        <source>NOT</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HelpWidget</name>
    <message>
        <location filename="help_widget.ui" line="14"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HerbsDataModel</name>
    <message>
        <location filename="herbs_data_model.cpp" line="251"/>
        <source>Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs_data_model.cpp" line="253"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs_data_model.cpp" line="255"/>
        <source>Actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="herbs_data_model.cpp" line="257"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ItemActionsToolBar</name>
    <message>
        <location filename="item_actions_tool_bar.cpp" line="48"/>
        <source>Exclude mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="item_actions_tool_bar.cpp" line="51"/>
        <source>Combine mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="item_actions_tool_bar.cpp" line="54"/>
        <source>Exclude and combine mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="item_actions_tool_bar.cpp" line="60"/>
        <source>Mark as bad combination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="item_actions_tool_bar.cpp" line="63"/>
        <source>Select item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="item_actions_tool_bar.cpp" line="66"/>
        <source>Mark as bad and set property for all
selected items except main item</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Life is Feudal Alchemy Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="34"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="41"/>
        <source>Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="47"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="57"/>
        <source>toolBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="68"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="73"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="84"/>
        <source>Enable filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="89"/>
        <source>Reset bad combinations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="94"/>
        <source>Language Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="99"/>
        <source>Recalc missed props</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="104"/>
        <source>View help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="45"/>
        <location filename="mainwindow.cpp" line="340"/>
        <source>Filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="65"/>
        <location filename="mainwindow.cpp" line="341"/>
        <source>Clear bad combinations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="164"/>
        <source>Open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="164"/>
        <source>Alchemy files (*.alc)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="171"/>
        <source>Opening error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="171"/>
        <source>Can&apos;t open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="181"/>
        <source>Save file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="188"/>
        <source>Saving error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="188"/>
        <source>Can&apos;t save file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="198"/>
        <source>Set language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="198"/>
        <location filename="mainwindow.cpp" line="200"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="198"/>
        <location filename="mainwindow.cpp" line="202"/>
        <source>Russian</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PropDialog</name>
    <message>
        <location filename="prop_dialog.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="prop_dialog.cpp" line="10"/>
        <source>Pick properties from list</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
