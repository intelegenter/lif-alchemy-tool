#ifndef PROP_DIALOG_H
#define PROP_DIALOG_H

#include <QDialog>
#include "herbs.h"
#include "propset.h"

class PropsModel;

namespace Ui {
class PropDialog;
}

class PropDialog : public QDialog
{
  Q_OBJECT

public:
  explicit PropDialog(const Props::Set& props, QWidget *parent = nullptr);
  ~PropDialog();
  Props::Set props();

private:
  Ui::PropDialog *ui;
  PropsModel* mModel;
};

#endif // PROP_DIALOG_H
