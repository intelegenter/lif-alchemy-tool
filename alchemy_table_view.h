#ifndef ALCHEMYTABLEVIEW_H
#define ALCHEMYTABLEVIEW_H

#include <QTableView>

class ItemActionsToolBar;
class AlchemyTableView : public QTableView
{
  Q_OBJECT
public:
  AlchemyTableView(QWidget* parent = nullptr);
  ~AlchemyTableView();
  void addItemActionToolbars(QList<ItemActionsToolBar*> toolbars);


  // QAbstractItemView interface
protected slots:
  void updateGeometries() override;

private:
  QList<ItemActionsToolBar*> mItemActionToolbars;
  QPair<int, QModelIndex>* mDesirableItemPos = nullptr;
  void scrollVerticalToOffset(const QModelIndex& ind, int verticalOffset);

  // QWidget interface
protected:
  void changeEvent(QEvent *) override;
};

#endif // ALCHEMYTABLEVIEW_H
