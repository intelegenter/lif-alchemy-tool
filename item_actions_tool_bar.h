#ifndef ITEMACTIONSTOOLBAR_H
#define ITEMACTIONSTOOLBAR_H

#include "enums.h"

#include <QToolBar>
#include <QModelIndex>

class HerbsDataModel;
class ItemActionsToolBar : public QToolBar
{
  Q_OBJECT
public:
  ItemActionsToolBar(QWidget* parent = nullptr,
                     HerbsDataModel* model = nullptr,
                     const QModelIndex& index = QModelIndex());
  void setModel(HerbsDataModel* model, const QModelIndex &index);
  QModelIndex index() const;
  void retranslate();

signals:
  void itemWantsToBeGood(const QModelIndex& index);
  void itemWantsToBeBadAndSet(const QModelIndex& index);
  void actionsChanged();
  void itemStateAboutToBeChanged(const QModelIndex& index);
  void itemStateAboutToBeReset(const QModelIndex& index);

private:
  QHash<Enums::Action, QAction*> mActions;
  QHash<QAction*, QAction*> mSeparators;
  QModelIndex mIndex;
  HerbsDataModel* mModel;
  bool dataChangeInProcess = false;

private:
  void setupUi();
  void onModelDataChanged(const QModelIndex &topLeft,
                          const QModelIndex &bottomRight,
                          const QList<int> &roles = QList<int>());
  void onModelReset();
  void loadDataFromModel(const QList<int> &roles);
  void connectActions();

  // QWidget interface
protected:
  void changeEvent(QEvent *) override;
};



#endif // ITEMACTIONSTOOLBAR_H
