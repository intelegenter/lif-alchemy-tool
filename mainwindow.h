#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidgetItem>
#include "herbs.h"
#include "enums.h"
#include "propset.h"
#include <map>
#include <QTranslator>
#include <QSortFilterProxyModel>
#include <QStandardItemModel>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class ProxyModel;
class HerbsDataModel;
class HerbProxyModel;
class ItemActionsToolBar;
class QSettings;
class PropsColumnDelegate;
class MainWindow : public QMainWindow
{
  Q_OBJECT

public:

  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

private slots:
  void onHerbItemDoubleClicked(const QModelIndex& index);
  void on_loadButton_clicked();
  void on_saveButton_clicked();
  void on_langButton_clicked();
  void on_createNew_clicked();
  void on_help_clicked();

  QByteArray getSavedData();
  bool loadFile(const QString& path);
  void setLoadedData(QByteArray data);

protected:
  void changeEvent(QEvent *event) override;
  void onGoodItemEvent(const QModelIndex& index);
  void onBadAndSetItemEvent(const QModelIndex& index);
  void updateHighlightedProps(Props::Set props);
  void appendRescentPath(const QString& path);
  void loadRescentFromSettings();
  void loadSettings();

private:
  void fillModelByEmptyData();
  ItemActionsToolBar* makeActionsWidget(const QModelIndex& index, QWidget *parent);
  void fillModelWidgets();
  Ui::MainWindow* ui = nullptr;
  HerbsDataModel* mModel = nullptr;
  HerbProxyModel* mProxy = nullptr;
  Enums::lang m_currLang = Enums::eng;
  QTranslator* m_currTranslator = nullptr;
  QString m_currFile = "";
  QAction* mFiltersAction = nullptr;
  QAction* mClearBadCombAction = nullptr;
  QSettings* mSettings = nullptr;
  QMenu* mRescentMenu = nullptr;
  PropsColumnDelegate* mPropsColumnDelegate;

  //helpers
  QVariant mClickedItemPos;

  // QWidget interface
protected:
  void closeEvent(QCloseEvent *event) override;

public:
  static inline const QString sGeometryKey = "geometry";
  static inline const QString sRescentArrayKey = "rescent_arr";
  static inline const QString sLanguageKey = "language";
  static inline const int sMaxRescentCount = 3;

};
#endif // MAINWINDOW_H
