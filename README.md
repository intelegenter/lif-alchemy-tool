# LIF Alchemy tool

you can do two things in a program:
1) assign properties to herbs
2) filter herbs by properties

The program has two localizations: English and Russian. They are switched by the lower left button

this allows you to use a different approach than brute force.
there are 18 properties and 91 reagents. a complete enumeration of all reagents in pairs gives a crazy number of combinations. on the other hand, if you have herbs with each of the 18 properties, then a full enumeration of the properties will take 91 * 18 = 1638 combinations in the worst case.
The algorithm is as follows: you have a herb, the properties of which you do not know. But you have herbs with all 18 properties.
You set a filter for the first property in the program, and it shows which herbs have this property. You need to take an unknown herb and mix with one of the known ones with this property. Repeat this for the other 18 properties.

You can save the results of your research in order to continue them at another time.
