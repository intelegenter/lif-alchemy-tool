#ifndef HERBSDATAMODEL_H
#define HERBSDATAMODEL_H

#include <QAbstractTableModel>

#include "enums.h"
#include "propset.h"

using RolesRemapMap = QMap<int, int>;

class HerbsDataModel : public QAbstractTableModel
{
  Q_OBJECT
public:
  enum Columns
  {
    ColumnIcon,
    ColumnName,
    ColumnActions,
    ColumnDiscoveredProps,
    ColumnLast
  };

  explicit HerbsDataModel(QObject *parent = nullptr);
public:
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &ind, int role) const override;
  bool setData(const QModelIndex &ind, const QVariant &value, int role) override;
  bool insertColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;
  bool removeColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;
  Qt::ItemFlags flags(const QModelIndex &index) const override;
  QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  QList<QModelIndex> getSelectedItems();
  bool isSelected(const QModelIndex& index);
  void appendRow(QMap<int, QVariant>& item);
  void setMode(Enums::WorkMode mode, const QModelIndex& firstItem = QModelIndex());
  void clear();
  QModelIndex firstItem() const;
  Enums::WorkMode mode() const;
  QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
  void clearBadCombinations();

  void loadData(QJsonDocument doc);
  QJsonDocument saveData();

signals:
  void modeChanged(Enums::WorkMode mode);
  void goodItemEvent(const QModelIndex& index);
  void selectItemEvent(const QModelIndex& index);
  void badItemEvent(const QModelIndex& index);
  void badAndSetItemEvent(const QModelIndex& index);

private:
  QMap<Columns, RolesRemapMap> mColumnsRolesRemaps;
  Enums::WorkMode mMode = Enums::manualMode;
  QList<QMap<int, QVariant>> mData;
  QModelIndex mFirstItem;
};



#endif // HERBSDATAMODEL_H
