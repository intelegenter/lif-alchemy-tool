#include "prop_dialog.h"
#include "ui_prop_dialog.h"
#include "props_model.h"

PropDialog::PropDialog(const Props::Set& props, QWidget *parent) :
  QDialog(parent),
  ui(new Ui::PropDialog)
{
  ui->setupUi(this);
  this->setWindowTitle(tr("Pick properties from list"));
  mModel = new PropsModel(ui->treeView);
  mModel->setColumnCount(1);
  QSet<Qt::CheckState> statesSet;
  for(int i = 0; i < Herb::Properties::LastProperty; i++)
  {
    auto prop = Herb::Properties(i);
    auto propItem = new QStandardItem(qApp->translate("Constants", Herb::propToStr.at(prop)));
    propItem->setCheckable(true);
    propItem->setAutoTristate(true);
    propItem->setCheckState(props.contains(prop) ? Qt::Checked : Qt::Unchecked);
    statesSet << propItem->checkState();
    propItem->setData(prop);
    mModel->appendRow(propItem);
  }
  ui->treeView->setModel(mModel);
  connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &PropDialog::accept);
  connect(ui->buttonBox, &QDialogButtonBox::rejected, this, &PropDialog::reject);
  //костыль ебаный, лень разбираться:
  ui->treeView->header()->hide();
}

PropDialog::~PropDialog()
{
  delete ui;
}

Props::Set PropDialog::props()
{
  Props::Set ret;
  for(int r = 0; r < mModel->rowCount(); r++)
  {
    auto item = mModel->item(r);
    if(item->checkState() == Qt::Checked)
      ret << item->data().value<Herb::Properties>();
  }
  return ret;
}
