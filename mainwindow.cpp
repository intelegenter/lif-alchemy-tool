#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "filtersdialog.h"
#include <QMessageBox>
#include <QMetaEnum>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QFileDialog>
#include <QInputDialog>
#include <QStyledItemDelegate>
#include <QMouseEvent>
#include <QContextMenuEvent>
#include <QSettings>

#include "prop_dialog.h"
#include "herbs_data_model.h"
#include "herb_proxy_model.h"
#include "props_column_delegate.h"
#include "item_actions_tool_bar.h"
#include "help_widget.h"

using namespace Enums;

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);

  for(auto& a : ui->menuBar->actions())
  {
    if(auto menu = a->menu())
    {
      menu->setAttribute(Qt::WA_TranslucentBackground);
      menu->setWindowFlag(Qt::FramelessWindowHint);
    }
  }

  QFile style(":/resources/style/AlchemyUiStyle.qss");
  if(style.open(QIODevice::ReadOnly))
    setStyleSheet(style.readAll());

  loadSettings();

  mModel = new HerbsDataModel(this);
  mProxy = new HerbProxyModel(mModel, this);
  connect(mProxy, &HerbProxyModel::highlightedPropSetChanged, this, &MainWindow::updateHighlightedProps);
  ui->tableView->setModel(mProxy);
  mPropsColumnDelegate = new PropsColumnDelegate(ui->tableView);
  ui->tableView->setItemDelegateForColumn(HerbsDataModel::ColumnDiscoveredProps, mPropsColumnDelegate);
  ui->tableView->horizontalHeader()->setStretchLastSection(true);
  ui->tableView->horizontalHeader()->setMinimumSectionSize(0);
  ui->tableView->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectRows);
  ui->tableView->setSelectionMode(QAbstractItemView::SelectionMode::NoSelection);
  ui->tableView->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
  ui->tableView->setWordWrap(false);

  mFiltersAction = new QAction(tr("Filters"));
  ui->toolBar->addAction(mFiltersAction);
  connect(mFiltersAction, &QAction::triggered, this,
          [this]()
          {
            auto dial = new FiltersDialog(mProxy->andFilter(),
                                          mProxy->orFilter(),
                                          mProxy->strictOrFilter(),
                                          mProxy->notFilter(),
                                          mModel->mode() != Enums::manualMode);
            connect(dial, &FiltersDialog::accepted, this,
                    [this, dial]()
                    {
                      mProxy->setManualFilters(dial->getAndFilters(),
                                               dial->getOrFilters(),
                                               dial->getStrictOrFilters(),
                                               dial->getNotFilters());
                    });
            dial->show();
          });
  mClearBadCombAction = new QAction(tr("Clear bad combinations"));
  ui->toolBar->addAction(mClearBadCombAction);
  connect(mClearBadCombAction, &QAction::triggered, mModel, &HerbsDataModel::clearBadCombinations);
  connect(ui->actionLanguageSettings, &QAction::triggered, this, &MainWindow::on_langButton_clicked);
  connect(ui->actionView_help, &QAction::triggered, this, &MainWindow::on_help_clicked);
  connect(ui->actionSave, &QAction::triggered, this, &MainWindow::on_saveButton_clicked);
  connect(ui->actionOpen, &QAction::triggered, this, &MainWindow::on_loadButton_clicked);
  connect(ui->actionCreate_new, &QAction::triggered, this, &MainWindow::on_createNew_clicked);

  connect(ui->tableView, &QTableView::doubleClicked, this, &MainWindow::onHerbItemDoubleClicked);
  connect(mModel, &HerbsDataModel::goodItemEvent, this, &MainWindow::onGoodItemEvent);
  connect(mModel, &HerbsDataModel::badAndSetItemEvent, this, &MainWindow::onBadAndSetItemEvent);

  fillModelByEmptyData();


  connect(mProxy, &QSortFilterProxyModel::layoutChanged, this,
          [this]()
          {

          });
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::fillModelByEmptyData()
{
  mModel->clear();

  for(int h = 0; h < Herb::Herbs::HerbLast; h++)
  {
    auto herb = (Herb::Herbs)h;
    QMap<int, QVariant> herbItem;
    herbItem[iconRole] = QVariant::fromValue(QPixmap(herbConsts::herbToImgPath.at(herb)));
    herbItem[herbEnumValueRole] = herb;
    herbItem[nameRole] = tr(herbConsts::herbToStr.at(herb));
    herbItem[propertiesRole] = QVariant::fromValue(Props::Set());
    herbItem[badCombinationsRole] = QVariant::fromValue(BCSet());
    mModel->appendRow(herbItem);
  }
  ui->tableView->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
  fillModelWidgets();
  ui->tableView->verticalHeader()->resizeSections(QHeaderView::ResizeToContents);
}

ItemActionsToolBar *MainWindow::makeActionsWidget(const QModelIndex &index, QWidget* parent)
{
  auto widget = new ItemActionsToolBar(parent, mModel, index);
  connect(widget, &ItemActionsToolBar::itemWantsToBeGood, this, &MainWindow::onGoodItemEvent);
  connect(widget, &ItemActionsToolBar::itemWantsToBeBadAndSet, this, &MainWindow::onBadAndSetItemEvent);
  connect(widget, &ItemActionsToolBar::actionsChanged, this,
          [this, widget]()
          {
            if(ui->tableView->horizontalHeader()->sectionSize(HerbsDataModel::ColumnActions) >= widget->sizeHint().width())
              return;
            ui->tableView->horizontalHeader()->
              resizeSection(HerbsDataModel::ColumnActions,
                            widget->sizeHint().width() + 15);
          });
  return widget;
}

void MainWindow::fillModelWidgets()
{
  QList<ItemActionsToolBar*> toolbars;
  for(int r = 0; r < mModel->rowCount(); r++)
  {
    auto index = mModel->index(r, HerbsDataModel::Columns::ColumnActions);
    auto widget = makeActionsWidget(index, ui->tableView->viewport());
    toolbars << widget;
  }
  ui->tableView->addItemActionToolbars(toolbars);
  ui->tableView->horizontalHeader()->
    resizeSection(HerbsDataModel::ColumnActions,
                  toolbars.first()->sizeHint().width() + 15);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
  mSettings->setValue(sGeometryKey, saveGeometry());
  mSettings->setValue(sLanguageKey, m_currLang);
}

void MainWindow::onHerbItemDoubleClicked(const QModelIndex &proxyIndex)
{
  if(proxyIndex.isValid())
  {
    if(proxyIndex.column() == HerbsDataModel::ColumnDiscoveredProps)
    {
      auto props = proxyIndex.data(Enums::propertiesRole).value<Props::Set>();
      auto pd = new PropDialog(props);
      connect(pd, &PropDialog::accepted, this, [proxyIndex, pd, props, this]()
              {
                auto sourceIndex = mProxy->mapToSource(proxyIndex);
                if(mModel->mode() == Enums::WorkMode::excMode)
                {
                  auto newlyAddedProps = pd->props();
                  newlyAddedProps.subtract(props);

                  auto props = mModel->data(sourceIndex, Enums::propertiesRole).value<Props::Set>();
                  mModel->setData(sourceIndex, QVariant::fromValue(props.unite(newlyAddedProps)), Enums::propertiesRole);

                  if(proxyIndex.row() != mModel->firstItem().row() &&
                      mModel->isSelected(sourceIndex))
                  {
                    props = mModel->data(mModel->firstItem(), Enums::propertiesRole).value<Props::Set>();
                    mModel->setData(mModel->firstItem(), QVariant::fromValue(props.unite(newlyAddedProps)), Enums::propertiesRole);
                  }
                }
                else
                {
                  mModel->setData(sourceIndex, QVariant::fromValue(pd->props()), Enums::propertiesRole);
                }
              });
      pd->show();
      return;
    }
  }
}

void MainWindow::on_loadButton_clicked()
{
  QString filePath = QFileDialog::getOpenFileName(this, tr("Open file"), "", tr("Alchemy files (*.alc)"));
  if(loadFile(filePath))
    appendRescentPath(filePath);
}

void MainWindow::on_saveButton_clicked()
{
  QString filePath = QFileDialog::getSaveFileName(this, tr("Save file"), "", "Alchemy files (*.alc)");
  if(filePath.isEmpty())
    return;

  QFile file(filePath);
  if(!file.open(QIODevice::WriteOnly))
  {
    QMessageBox::critical(this, tr("Saving error"), tr("Can't save file"));
    return;
  }
  file.write(getSavedData());
  file.close();
  appendRescentPath(filePath);
}

void MainWindow::on_langButton_clicked()
{
  bool ok;
  QString str = QInputDialog::getItem(this, tr("Set language"), "", {tr("English"), tr("Russian")}, (int)m_currLang, false, &ok);
  lang rez;
  if(str == tr("English"))
    rez = eng;
  else if(str == tr("Russian"))
    rez = ru;
  if(rez == m_currLang)
    return;
  switch (rez) {
  case lang::ru:
  {
    if(m_currTranslator)
    {
      qApp->removeTranslator(m_currTranslator);
      delete m_currTranslator;
    }
    m_currTranslator = new QTranslator(this);
    if(!m_currTranslator->load(":/ru.qm"))
      assert(false);
    qApp->installTranslator(m_currTranslator);
    m_currLang = ru;
    setLoadedData(getSavedData());
    break;
  }
  case lang::eng:
  {
    if(m_currTranslator)
    {
      qApp->removeTranslator(m_currTranslator);
      delete m_currTranslator;
      m_currTranslator = nullptr;
    }
    m_currTranslator = new QTranslator(this);
    if(!m_currTranslator->load(":/en.qm"))
      assert(false);
    qApp->installTranslator(m_currTranslator);
    m_currLang = eng;
    setLoadedData(getSavedData());
    break;
  }
  default:
    break;
  }
}

void MainWindow::on_createNew_clicked()
{
  mModel->clear();
  fillModelByEmptyData();
  setWindowTitle(tr("Life is Feudal Alchemy Tool"));
}

void MainWindow::on_help_clicked()
{
  auto w = new HelpWidget(m_currLang);
  w->show();
}

QByteArray MainWindow::getSavedData()
{
  return mModel->saveData().toJson();
}

bool MainWindow::loadFile(const QString &path)
{
  if(path.isEmpty())
    return false;

  QFile file(path);
  if(!file.open(QIODevice::ReadOnly))
  {
    QMessageBox::critical(this, tr("Opening error"), tr("Can't open file"));
    return false;
  }
  setWindowTitle(tr("Life is Feudal Alchemy Tool") + " - " + path);
  setLoadedData(file.readAll());
  file.close();
  return true;
}

void MainWindow::setLoadedData(QByteArray data)
{
  mModel->loadData(QJsonDocument::fromJson(data));
//  ui->tableView->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
  ui->tableView->verticalHeader()->resizeSections(QHeaderView::ResizeToContents);
}

void MainWindow::changeEvent(QEvent *event)
{
  if (event->type() == QEvent::LanguageChange)
  {
    ui->retranslateUi(this);
    mFiltersAction->setText(tr("Filters"));
    mClearBadCombAction->setText(tr("Clear bad combinations"));
    mRescentMenu->setTitle(tr("Rescent"));
  }
  QMainWindow::changeEvent(event);
}

void MainWindow::onGoodItemEvent(const QModelIndex &index)
{
  if(mModel->mode() != Enums::excCombMode)
    return;
  auto firstItemInd = mModel->firstItem();
  auto oldProps = index.data(Enums::propertiesRole).value<Props::Set>();
  auto pd = new PropDialog(Props::Set());
  connect(pd, &PropDialog::accepted, this, [index, firstItemInd, pd, oldProps, this]()
          {
            auto newProps = pd->props();
            auto firstProps = firstItemInd.data(Enums::propertiesRole).value<Props::Set>();
            firstProps.unite(newProps);
            mModel->setData(index, QVariant::fromValue(pd->props().unite(oldProps)), Enums::propertiesRole);
            mModel->setData(firstItemInd, QVariant::fromValue(firstProps), Enums::propertiesRole);
            mModel->setData(index, Enums::isGood, Enums::itemStateRole);
          });
  connect(pd, &PropDialog::rejected, this, [index, firstItemInd, pd, oldProps, this]()
          {
            emit mModel->dataChanged(index, index, {Enums::itemStateRole});
//            mModel->setData(index, index.data(Enums::itemStateRole), Enums::itemStateRole);
          });
  pd->show();
}

void MainWindow::onBadAndSetItemEvent(const QModelIndex &index)
{
  if(mModel->mode() != Enums::excCombMode)
    return;
  auto firstItemInd = mModel->firstItem();
  auto oldProps = index.data(Enums::propertiesRole).value<Props::Set>();
  auto pd = new PropDialog(Props::Set());
  connect(pd, &PropDialog::accepted, this, [index, pd, oldProps, this]()
          {
            mModel->setData(index, QVariant::fromValue(pd->props().unite(oldProps)), Enums::propertiesRole);
            mModel->setData(index, Enums::isBad, Enums::itemStateRole);
            for(auto& ind : mModel->getSelectedItems())
            {
              auto props = ind.data(Enums::propertiesRole).value<Props::Set>();
              mModel->setData(ind, QVariant::fromValue(pd->props().unite(props)), Enums::propertiesRole);
              mModel->setData(ind, Enums::isBad, Enums::itemStateRole);
            }
          });
  pd->show();
}

void MainWindow::updateHighlightedProps(Props::Set props)
{
  mPropsColumnDelegate->setHighlightedProps(props);
  for(int r = 0; r < mProxy->rowCount(); r++)
    ui->tableView->update(mProxy->index(0, HerbsDataModel::Columns::ColumnDiscoveredProps));
}

void MainWindow::appendRescentPath(const QString &path)
{
  QStringList rescentList = mSettings->value(sRescentArrayKey).toStringList();
  auto iter = std::find(rescentList.begin(), rescentList.end(), path);
  if(iter != rescentList.end())
    rescentList.move(std::distance(rescentList.begin(), iter), 0);
  else
    rescentList.prepend(path);
  if(rescentList.size() > sMaxRescentCount)
    rescentList.resize(sMaxRescentCount);

  mSettings->setValue(sRescentArrayKey, rescentList);
  loadRescentFromSettings();
}

void MainWindow::loadRescentFromSettings()
{
  if(!mRescentMenu)
  {
    QMenu* fileMenu = nullptr;
    for(auto& a : ui->menuBar->actions())
      if(auto menu = a->menu())
        if(menu->objectName() == "menuFile")
          fileMenu = menu;

    if(fileMenu)
    {
      mRescentMenu = new QMenu(tr("Rescent"), this);
      mRescentMenu->setAttribute(Qt::WA_TranslucentBackground);
      mRescentMenu->setWindowFlag(Qt::FramelessWindowHint);
      fileMenu->addMenu(mRescentMenu);
      mRescentMenu->setVisible(false);
    }
  }
  mRescentMenu->clear();
  auto rescentList = mSettings->value(sRescentArrayKey).toStringList();
  if(rescentList.isEmpty())
  {
    mRescentMenu->setVisible(false);
  }
  else
  {
    mRescentMenu->setVisible(true);
    for(auto& rescent : rescentList)
    {
      auto action = mRescentMenu->addAction(rescent);
      connect(action, &QAction::triggered, this,
              [this, rescent]()
              {
                loadFile(rescent);
                appendRescentPath(rescent);
              });
    }
  }
  mRescentMenu->hide();
}

void MainWindow::loadSettings()
{
  mSettings = new QSettings("userSettings.ini", QSettings::Format::IniFormat, this);
  if(mSettings->value(sGeometryKey).isValid())
    restoreGeometry(mSettings->value(sGeometryKey).toByteArray());

  m_currLang = (Enums::lang)mSettings->value(sLanguageKey, 0).toInt();
  QString translatorPath;
  switch(m_currLang)
  {
  case eng:
    translatorPath = ":/en.qm";
    break;
  case ru:
    translatorPath = ":/ru.qm";
    break;
  }

  m_currTranslator = new QTranslator(this);

  if(!m_currTranslator->load(translatorPath))
    assert(false);
  qApp->installTranslator(m_currTranslator);

  loadRescentFromSettings();
}
