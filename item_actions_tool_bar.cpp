#include "item_actions_tool_bar.h"
#include "herbs_data_model.h"
#include <QToolButton>
#include <QPaintEvent>

ItemActionsToolBar::ItemActionsToolBar(QWidget *parent, HerbsDataModel *model, const QModelIndex &index)
  : QToolBar(parent),
  mModel(nullptr)
{
  setupUi();
  setModel(model, index);
}

void ItemActionsToolBar::setModel(HerbsDataModel *model, const QModelIndex &index)
{
  if(mModel)
    disconnect(mModel, nullptr, this, nullptr);
  for(auto&a : mActions)
    disconnect(a, nullptr, nullptr, nullptr);
  mModel = model;
  mIndex = index;
  if(!mModel || !mIndex.isValid())
    return;

  connect(mModel, &HerbsDataModel::dataChanged, this, &ItemActionsToolBar::onModelDataChanged);
  connect(mModel, &HerbsDataModel::modelReset, this, &ItemActionsToolBar::onModelReset);
  connectActions();

  loadDataFromModel({Enums::availableActionsRole,
                     Enums::itemStateRole});
}

QModelIndex ItemActionsToolBar::index() const
{
  return mIndex;
}

void ItemActionsToolBar::retranslate()
{
  for(auto& a : actions())
  {
    auto trKey = a->property("translation_key_prop");
    if(trKey.isValid())
      a->setToolTip(tr(trKey.toString().toStdString().c_str()));
  }
}

void ItemActionsToolBar::setupUi()
{
  setMinimumSize(20,20);
  setStyleSheet(/*"QToolButton {background: none;}"*/
                "QToolButton {border-radius: 4px; background: none;}"
                "QToolButton:checked {background: #E0B115;}"
                "QToolButton:hover {background: #7D7E75;}"
                );
  setToolButtonStyle(Qt::ToolButtonIconOnly);
  auto enterExcAction = new QAction(QIcon(":/resources/icons/exclude.png"), "E", this);
  enterExcAction->setCheckable(true);
  enterExcAction->setProperty("translation_key_prop", "Exclude mode");
  enterExcAction->setToolTip(tr("Exclude mode"));
  auto enterCombAction = new QAction(QIcon(":/resources/icons/combine.png"), "C", this);
  enterCombAction->setCheckable(true);
  enterCombAction->setProperty("translation_key_prop", "Combine mode");
  enterCombAction->setToolTip(tr("Combine mode"));
  auto enterExcCombAction = new QAction(QIcon(":/resources/icons/excludeAndCombine.png"), "E&&C", this);
  enterExcCombAction->setCheckable(true);
  enterExcCombAction->setProperty("translation_key_prop", "Exclude and combine mode");
  enterExcCombAction->setToolTip(tr("Exclude and combine mode"));
  auto goodAction = new QAction(QIcon(":/resources/icons/checkmark.png"), "G", this);
  goodAction->setCheckable(true);
  goodAction->setProperty("translation_key_prop", "Mark as good combination");
  goodAction->setToolTip(tr("Mark as good combination"));
  auto badAction = new QAction(QIcon(":/resources/icons/cross.png"), "B", this);
  badAction->setCheckable(true);
  badAction->setProperty("translation_key_prop", "Mark as bad combination");
  badAction->setToolTip(tr("Mark as bad combination"));
  auto selectAction = new QAction(QIcon(":/resources/icons/bookmark.png"), "S", this);
  selectAction->setCheckable(true);
  selectAction->setProperty("translation_key_prop", "Select item");
  selectAction->setToolTip(tr("Select item"));
  auto badAndSetAction = new QAction(QIcon(":/resources/icons/badAndSelect.png"), "B&&A", this);
  badAndSetAction->setCheckable(false);
  badAndSetAction->setProperty("translation_key_prop", "Mark as bad and set property for all\n"
                                               "selected items except main item");
  badAndSetAction->setToolTip(tr("Mark as bad and set property for all\n"
                                 "selected items except main item"));

  mActions[Enums::enterExcModeAction] = enterExcAction;
  mActions[Enums::enterCombModeAction] = enterCombAction;
  mActions[Enums::enterExcCombModeAction] = enterExcCombAction;
  mActions[Enums::goodCombAction] = goodAction;
  mActions[Enums::badCombAction] = badAction;
  mActions[Enums::selectAction] = selectAction;
  mActions[Enums::badAndSetAction] = badAndSetAction;
  addAction(enterExcAction);
  mSeparators[enterCombAction] = addSeparator();
  addAction(enterCombAction);
  mSeparators[enterExcCombAction] = addSeparator();
  addAction(enterExcCombAction);
  addAction(goodAction);
  mSeparators[badAction] = addSeparator();
  addAction(badAction);
  mSeparators[selectAction] = addSeparator();
  addAction(selectAction);
  mSeparators[badAndSetAction] = addSeparator();
  addAction(badAndSetAction);

  for(auto& a : mActions)
    a->setVisible(false);
  for(auto& s : mSeparators)
    s->setVisible(false);
  for(auto& tb : findChildren<QToolButton*>())
    tb->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
}

void ItemActionsToolBar::onModelDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QList<int> &roles)
{
  if(mIndex.row() < topLeft.row() ||
      mIndex.row() > bottomRight.row())
    return;
  loadDataFromModel(roles);
}

void ItemActionsToolBar::onModelReset()
{
  if(mModel->rowCount() == 0)
  {
    mIndex = QModelIndex();
    return;
  };

  auto newInd = mModel->index(mIndex.row(), mIndex.column());
  if(newInd.isValid())
  {
    mIndex = newInd;
    loadDataFromModel({Enums::availableActionsRole,
                       Enums::itemStateRole});
  }
}

void ItemActionsToolBar::loadDataFromModel(const QList<int> &roles)
{
  if(roles.contains(Enums::availableActionsRole))
  {
    auto availableActions = mIndex.data(Enums::availableActionsRole).value<Enums::Actions>();
    for(auto iter = mActions.keyValueBegin();
         iter != mActions.keyValueEnd();
         iter++)
    {
      if(availableActions.testFlag(iter->first))
      {
        iter->second->setVisible(true);
        if(mSeparators.contains(iter->second))
        {
          if(availableActions != Enums::Action::selectAction)
            mSeparators[iter->second]->setVisible(true);
          else
          {
            //do nothing
            //in that case we have only one (SEL) action,
            //and no need to show any separators
          }
        }
      }
      else
      {
        iter->second->setVisible(false);
        if(mSeparators.contains(iter->second))
          mSeparators[iter->second]->setVisible(false);
      }
    }
  }
  if(roles.contains(Enums::itemStateRole))
  {
    auto state = mIndex.data(Enums::itemStateRole).value<Enums::ItemState>();
    for(auto& a : mActions)
    {
      if(a->isChecked())
      {
        QSignalBlocker b(a);
        a->setChecked(false);
      }
    }
    switch(state)
    {
    case Enums::isGood:
    {
      auto a = mActions[Enums::goodCombAction];
      if(!a->isChecked())
      {
        QSignalBlocker b(a);
        a->setChecked(true);
      }
      break;
    }
    case Enums::isBad:
    {
      auto a = mActions[Enums::badCombAction];
      if(!a->isChecked())
      {
        QSignalBlocker b(a);
        a->setChecked(true);
      }
      break;
    }
    case Enums::isSelect:
    {
      auto a = mActions[Enums::selectAction];
      if(!a->isChecked())
      {
        QSignalBlocker b(a);
        a->setChecked(true);
      }
      break;
    }
    case Enums::isExcMode:
    {
      auto a = mActions[Enums::enterExcModeAction];
      if(!a->isChecked())
      {
        QSignalBlocker b(a);
        a->setChecked(true);
      }
      break;
    }
    case Enums::isCombMode:
    {
      auto a = mActions[Enums::enterCombModeAction];
      if(!a->isChecked())
      {
        QSignalBlocker b(a);
        a->setChecked(true);
      }
      break;
    }
    case Enums::isExcCombMode:
    {
      auto a = mActions[Enums::enterExcCombModeAction];
      if(!a->isChecked())
      {
        QSignalBlocker b(a);
        a->setChecked(true);
      }
      break;
    }
    }
  }
  emit actionsChanged();
}

void ItemActionsToolBar::connectActions()
{
  auto connectActionStateChanging =
    [this](QAction* action, Enums::ItemState state)
    {
      connect(action, &QAction::toggled, this,
              [this, state](bool checked)
              {
                emit itemStateAboutToBeChanged(mIndex);
                if(checked)
                  mModel->setData(mIndex, state, Enums::itemStateRole);
                else
                  mModel->setData(mIndex, QVariant(), Enums::itemStateRole);
              });
    };
  auto connectActionModelModeChanging =
    [this](QAction* action, Enums::WorkMode mode)
  {
      connect(action, &QAction::toggled, this,
              [this, mode](bool checked)
              {
                if(checked)
                  mModel->setMode(mode, mIndex);
                else
                {
                  emit itemStateAboutToBeReset(mIndex);
                  mModel->setMode(Enums::manualMode);
                }
              });
  };
  connectActionStateChanging(mActions[Enums::enterExcModeAction], Enums::isExcMode);
  connectActionModelModeChanging(mActions[Enums::enterExcModeAction], Enums::excMode);

  connectActionStateChanging(mActions[Enums::enterCombModeAction], Enums::isCombMode);
  connectActionModelModeChanging(mActions[Enums::enterCombModeAction], Enums::combMode);

  connectActionStateChanging(mActions[Enums::enterExcCombModeAction], Enums::isExcCombMode);
  connectActionModelModeChanging(mActions[Enums::enterExcCombModeAction], Enums::excCombMode);

  connectActionStateChanging(mActions[Enums::badCombAction], Enums::isBad);
  connectActionStateChanging(mActions[Enums::selectAction], Enums::isSelect);

  connect(mActions[Enums::goodCombAction], &QAction::toggled, this,
          [this](bool checked)
          {
            emit itemStateAboutToBeChanged(mIndex);
            if(!checked)
              mModel->setData(mIndex, QVariant(), Enums::itemStateRole);
            else
              emit itemWantsToBeGood(mIndex);
          });
  connect(mActions[Enums::badAndSetAction], &QAction::triggered, this,
          [this]()
          {
            emit itemStateAboutToBeChanged(mIndex);
            emit itemWantsToBeBadAndSet(mIndex);
          });
}

void ItemActionsToolBar::changeEvent(QEvent *)
{
  retranslate();
}
