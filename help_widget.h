#ifndef HELP_WIDGET_H
#define HELP_WIDGET_H

#include <QWidget>
#include "enums.h"

namespace Ui {
class HelpWidget;
}

class HelpWidget : public QWidget
{
  Q_OBJECT

public:
  explicit HelpWidget(Enums::lang language, QWidget *parent = nullptr);
  ~HelpWidget();

private:
  Ui::HelpWidget *ui;
};

#endif // HELP_WIDGET_H
