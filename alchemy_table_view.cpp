#include "alchemy_table_view.h"
#include "item_actions_tool_bar.h"
#include "herb_proxy_model.h"
#include <QScrollBar>
#include <QHeaderView>

AlchemyTableView::AlchemyTableView(QWidget *parent)
  : QTableView(parent)
{
  setVerticalScrollMode(ScrollMode::ScrollPerPixel);
  connect(verticalScrollBar(), &QScrollBar::valueChanged, this, [](int val){qDebug() << "scroll to" << val;});
}

AlchemyTableView::~AlchemyTableView()
{
  if(mDesirableItemPos)
    delete mDesirableItemPos;
  mDesirableItemPos = nullptr;
}

void AlchemyTableView::addItemActionToolbars(QList<ItemActionsToolBar *> toolbars)
{
  for(auto& tb : mItemActionToolbars)
    tb->deleteLater();
  mItemActionToolbars = toolbars;
  for(auto& tb : mItemActionToolbars)
  {
    tb->setParent(viewport());
    connect(tb, &ItemActionsToolBar::itemStateAboutToBeChanged, this,
            [this](const QModelIndex& index)
            {
              QModelIndex ind = index;
              if(auto m = qobject_cast<QSortFilterProxyModel*>(model()))
                ind = m->mapFromSource(index);
              if(ind.isValid())
              {
                if(mDesirableItemPos)
                  *mDesirableItemPos = {visualRect(ind).top() , index};
                else
                  mDesirableItemPos = new QPair<int, QModelIndex>(visualRect(ind).top(), index);
              }
            });
  }
  updateGeometries();
}

void AlchemyTableView::updateGeometries()
{
  QTableView::updateGeometries();
  for(auto& tb : mItemActionToolbars)
  {
    auto proxy = qobject_cast<HerbProxyModel*>(model());
    for(auto& tb : mItemActionToolbars)
    {
      auto index = tb->index();
      if(proxy)
        index = proxy->mapFromSource(index);
      if(index.isValid())
      {
        tb->setGeometry(visualRect(index));
        if(tb->isHidden())
          tb->show();
      }
      else
      {
        tb->hide();
      }
    }
  }
  if(mDesirableItemPos)
  {
    auto ind = mDesirableItemPos->second;

    if(auto m = qobject_cast<QSortFilterProxyModel*>(model()))
      ind = m->mapFromSource(ind);

    if(ind.isValid())
      scrollVerticalToOffset(ind, mDesirableItemPos->first);
  }
}

void AlchemyTableView::scrollVerticalToOffset(const QModelIndex &ind, int vOffset)
{
  if(ind.isValid())
  {
    int verticalPosition = verticalHeader()->sectionPosition(ind.row());
    int verticalIndex = verticalHeader()->visualIndex(ind.row());

    if (verticalScrollMode() == QAbstractItemView::ScrollPerItem)
    {
      {
        int h = vOffset;
        int y = verticalHeader()->sectionSize(ind.row());
        while (verticalIndex > 0)
        {
          int row = verticalHeader()->logicalIndex(verticalIndex - 1);
          y += verticalHeader()->sectionSize(row);
          if (y > h)
            break;
          --verticalIndex;
        }
      }

      {
        int hiddenSections = 0;
        if (verticalHeader()->sectionsHidden())
        {
          for (int s = verticalIndex - 1; s >= 0; --s)
          {
            int row = verticalHeader()->logicalIndex(s);
            if (verticalHeader()->isSectionHidden(row))
              ++hiddenSections;
          }
        }
        verticalScrollBar()->setValue(verticalIndex - hiddenSections);
      }

    }
    else
    { // ScrollPerPixel
      verticalScrollBar()->setValue(verticalPosition - vOffset);
    }

    update(ind);
  }
}

void AlchemyTableView::changeEvent(QEvent *)
{
  for(auto& tb : mItemActionToolbars)
  {
    tb->retranslate();
  }
}
