#ifndef FILTERSDIALOG_H
#define FILTERSDIALOG_H

#include <QDialog>
#include <QStandardItemModel>
#include "herbs.h"
#include "propset.h"

class PropsModel;
namespace Ui {
class FiltersDialog;
}

class FiltersDialog : public QDialog
{
  Q_OBJECT

public:
  explicit FiltersDialog(const Props::Set &andF,
                         const Props::Set &orF,
                         const Props::Set& strictOrF,
                         const Props::Set &notF,
                         bool disabled = false,
                         QWidget *parent = 0);
  ~FiltersDialog();
  Props::Set getAndFilters();
  Props::Set getOrFilters();
  Props::Set getStrictOrFilters();
  Props::Set getNotFilters();

protected:
  void changeEvent(QEvent *event);

private:
  Ui::FiltersDialog *ui;
  PropsModel* mModel;
  QStandardItem* mAndItem;
  QStandardItem* mOrItem;
  QStandardItem* mStrictOrItem;
  QStandardItem* mNotItem;
//  Props::Set mAndFilters;
//  Props::Set mOrFilters;
//  Props::Set mNotFilters;
};

#endif // FILTERSDIALOG_H
