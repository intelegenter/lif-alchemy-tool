<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>Constants</name>
    <message>
        <location filename="herbs.h" line="134"/>
        <source>Flux</source>
        <translation>Присадка</translation>
    </message>
    <message>
        <location filename="herbs.h" line="135"/>
        <source>Naphta</source>
        <translation>Нафта</translation>
    </message>
    <message>
        <location filename="herbs.h" line="136"/>
        <source>Food Flavour</source>
        <translation>Приправа</translation>
    </message>
    <message>
        <location filename="herbs.h" line="137"/>
        <source>Lowers max sHP</source>
        <translation>Понижает максимум сознания</translation>
    </message>
    <message>
        <location filename="herbs.h" line="138"/>
        <source>Damage sHP</source>
        <translation>Наносит урон сознанию</translation>
    </message>
    <message>
        <location filename="herbs.h" line="139"/>
        <source>Restore sHP</source>
        <oldsource>RestoresHP</oldsource>
        <translation>Восстанавливает сознание</translation>
    </message>
    <message>
        <location filename="herbs.h" line="140"/>
        <source>Damage hHP</source>
        <translation>Наносит урон здоровью</translation>
    </message>
    <message>
        <location filename="herbs.h" line="141"/>
        <source>Antidote</source>
        <translation>Антидот</translation>
    </message>
    <message>
        <location filename="herbs.h" line="142"/>
        <source>Damage hStam</source>
        <translation>Наносит урон марафонской выносливости</translation>
    </message>
    <message>
        <location filename="herbs.h" line="143"/>
        <source>Restore hStam</source>
        <translation>Восстанавливает марафонскую выносливость</translation>
    </message>
    <message>
        <location filename="herbs.h" line="144"/>
        <source>Damage sStam regen</source>
        <translation>Замедляет восстановление спринтерской выносливости</translation>
    </message>
    <message>
        <location filename="herbs.h" line="145"/>
        <source>Lowers max sStam</source>
        <translation>Понижает максимум спринтерской выносливости</translation>
    </message>
    <message>
        <location filename="herbs.h" line="146"/>
        <source>Raise max sStam</source>
        <translation>Повышает максимум спринтерской выносливости</translation>
    </message>
    <message>
        <location filename="herbs.h" line="147"/>
        <source>Raise Con</source>
        <translation>Повышает телосложение</translation>
    </message>
    <message>
        <location filename="herbs.h" line="148"/>
        <source>Raise Will</source>
        <translation>Повышает силу воли</translation>
    </message>
    <message>
        <location filename="herbs.h" line="149"/>
        <source>Raise Int</source>
        <translation>Повышает интеллект</translation>
    </message>
    <message>
        <location filename="herbs.h" line="150"/>
        <source>Raise Str</source>
        <translation>Повышает силу</translation>
    </message>
    <message>
        <location filename="herbs.h" line="151"/>
        <source>Raise Agi</source>
        <translation>Повышает ловкость</translation>
    </message>
    <message>
        <location filename="herbs.h" line="159"/>
        <source>Acerba Moretum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="160"/>
        <source>Adipem Nebulo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="161"/>
        <source>Albus Viduae</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="162"/>
        <source>Aquila Peccatum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="163"/>
        <source>Aureus Magistrum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="164"/>
        <source>Bacce Hamsa</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="165"/>
        <source>Burmenta Wallo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="166"/>
        <source>Caeci Custos</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="167"/>
        <source>Chorea Iram</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="168"/>
        <source>Curaila Jangha</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="169"/>
        <source>Curva Manus</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="170"/>
        <source>Desertus Smilax</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="171"/>
        <source>Dulcis Radix</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="172"/>
        <source>Dustali Krabo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="173"/>
        <source>Errantia Ludaeo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="174"/>
        <source>Fakha Rudob</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="175"/>
        <source>Falcem Malleorum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="176"/>
        <source>Fassari Tolge</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="177"/>
        <source>Filia Prati</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="178"/>
        <source>Fohatta Torn</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="179"/>
        <source>Fuskegtra Xelay</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="180"/>
        <source>Gortaka Messen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="181"/>
        <source>Gratias Sivara</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="182"/>
        <source>Hallatra Kronye</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="183"/>
        <source>Holmatu Stazo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="184"/>
        <source>Huryosa Gulla</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="185"/>
        <source>Ital Iranta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="186"/>
        <source>Jenaro Vannakam</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="187"/>
        <source>Jukola Beshaar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="188"/>
        <source>Kacaro Vilko</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="189"/>
        <source>Kaleda Mesgano</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="190"/>
        <source>Kalya Nori</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="191"/>
        <source>Khalari Gratsi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="192"/>
        <source>Kromenta Salicia</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="193"/>
        <source>Kurupa Andhere</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="194"/>
        <source>Kyasaga Sherl</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="195"/>
        <source>Laster Kutta</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="196"/>
        <source>Lobos Gamsa</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="197"/>
        <source>Mala Fugam</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="198"/>
        <source>Mauna Boba</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="199"/>
        <source>Memen Anik</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="200"/>
        <source>Mons Bastardus</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="201"/>
        <source>Muncha Vana</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="202"/>
        <source>Murkha Bola</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="203"/>
        <source>Naraen Pandanomo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="204"/>
        <source>Nequissimum Propodium</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="205"/>
        <source>Nocte Lumen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="206"/>
        <source>Oscularetur</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="207"/>
        <source>Pecuarius Ventus</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="208"/>
        <source>Persetu Hara</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="209"/>
        <source>Petra Stellam</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="210"/>
        <source>Phlavar Pharest</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="211"/>
        <source>Pitaku Koro</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="212"/>
        <source>Pungentibus Chorea</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="213"/>
        <source>Rakta Stema</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="214"/>
        <source>Remerta Poskot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="215"/>
        <source>Ripyote Quamisy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="216"/>
        <source>Rosa Kingsa</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="217"/>
        <source>Saltare Diabolus</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="218"/>
        <source>Sapienta Mantis</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="219"/>
        <source>Sarmento Gaute</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="220"/>
        <source>Suryodaya Bhagya</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="221"/>
        <source>Topasa Maidana</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="222"/>
        <source>Uliya Sundara</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="223"/>
        <source>Utrokka Khuru</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="224"/>
        <source>Vertato Zonda</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="225"/>
        <source>Viridi Ursae</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="226"/>
        <source>Aktashite</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="227"/>
        <source>Bixbyite</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="228"/>
        <source>Chrysoberyl</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="229"/>
        <source>Euclase</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="230"/>
        <source>Kutnohorite</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="231"/>
        <source>Meionite</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="232"/>
        <source>Scolecite</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="233"/>
        <source>Gahnite</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="234"/>
        <source>Tellurite</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="235"/>
        <source>Urea</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="236"/>
        <source>Humite</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="237"/>
        <source>Parsonsite</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="238"/>
        <source>Animal calculi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="239"/>
        <source>Blinded eye</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="240"/>
        <source>Bone meal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="241"/>
        <source>Crystalized bile</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="242"/>
        <source>Digested feather</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="243"/>
        <source>Glowing urine</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="244"/>
        <source>Odd claw</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="245"/>
        <source>Overgrown parasite</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="246"/>
        <source>Piece of diseased hide</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="247"/>
        <source>Purbon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="248"/>
        <source>Strange muscle</source>
        <translation></translation>
    </message>
    <message>
        <location filename="herbs.h" line="249"/>
        <source>Uncommon tendon</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FiltersDialog</name>
    <message>
        <location filename="filtersdialog.ui" line="14"/>
        <source>Select properties</source>
        <translation>Выберите свойства</translation>
    </message>
    <message>
        <location filename="filtersdialog.cpp" line="18"/>
        <source>Pick filters from lists</source>
        <translation>Выберите фильтры из списка</translation>
    </message>
    <message>
        <location filename="filtersdialog.cpp" line="40"/>
        <source>AND</source>
        <translation>И</translation>
    </message>
    <message>
        <location filename="filtersdialog.cpp" line="43"/>
        <source>OR</source>
        <translation>ИЛИ</translation>
    </message>
    <message>
        <location filename="filtersdialog.cpp" line="46"/>
        <source>STRICT OR</source>
        <translation>СТРОГИЙ ИЛИ</translation>
    </message>
    <message>
        <location filename="filtersdialog.cpp" line="49"/>
        <source>NOT</source>
        <translation>НЕ</translation>
    </message>
    <message>
        <source>Flux</source>
        <translation type="vanished">Присадка</translation>
    </message>
    <message>
        <source>Naphta</source>
        <translation type="vanished">Нафта</translation>
    </message>
    <message>
        <source>Food Flavour</source>
        <translation type="vanished">Приправа</translation>
    </message>
    <message>
        <source>Lowers max sHP</source>
        <translation type="vanished">Понижает максимум сознания</translation>
    </message>
    <message>
        <source>Damage sHP</source>
        <translation type="vanished">Наносит урон сознанию</translation>
    </message>
    <message>
        <source>Restore sHP</source>
        <translation type="vanished">Восстанавливает сознание</translation>
    </message>
    <message>
        <source>Damage hHP</source>
        <translation type="vanished">Наносит урон здоровью</translation>
    </message>
    <message>
        <source>Antidote</source>
        <translation type="vanished">Антидот</translation>
    </message>
    <message>
        <source>Damage hStam</source>
        <translation type="vanished">Наносит урон марафонской выносливости</translation>
    </message>
    <message>
        <source>Restore hStam</source>
        <translation type="vanished">Восстанавливает марафонскую выносливость</translation>
    </message>
    <message>
        <source>Damage sStam regen</source>
        <translation type="vanished">Ухудшает восстановление спринтерской выносливости</translation>
    </message>
    <message>
        <source>Lowers max sStam</source>
        <translation type="vanished">Понижает максимум спринтерской выносливости</translation>
    </message>
    <message>
        <source>Raise max sStam</source>
        <translation type="vanished">Повышает максимум спринтерской выносливости</translation>
    </message>
    <message>
        <source>Raise Con</source>
        <translation type="vanished">Повышает телосложение</translation>
    </message>
    <message>
        <source>Raise Will</source>
        <translation type="vanished">Повышает силу воли</translation>
    </message>
    <message>
        <source>Raise Int</source>
        <translation type="vanished">Повышает интеллект</translation>
    </message>
    <message>
        <source>Raise Str</source>
        <translation type="vanished">Повышает силу</translation>
    </message>
    <message>
        <source>Raise Agi</source>
        <translation type="vanished">Повышает ловкость</translation>
    </message>
</context>
<context>
    <name>HelpWidget</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Форма</translation>
    </message>
    <message>
        <location filename="help_widget.ui" line="14"/>
        <source>Manual</source>
        <translation>Руководство</translation>
    </message>
</context>
<context>
    <name>HerbsDataModel</name>
    <message>
        <location filename="herbs_data_model.cpp" line="258"/>
        <source>Icon</source>
        <translation>Иконка</translation>
    </message>
    <message>
        <location filename="herbs_data_model.cpp" line="260"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="herbs_data_model.cpp" line="262"/>
        <source>Actions</source>
        <translation>Действия</translation>
    </message>
    <message>
        <location filename="herbs_data_model.cpp" line="264"/>
        <source>Properties</source>
        <translation>Свойства</translation>
    </message>
</context>
<context>
    <name>ItemActionsToolBar</name>
    <message>
        <location filename="item_actions_tool_bar.cpp" line="60"/>
        <source>Exclude mode</source>
        <translation>Режим исключения</translation>
    </message>
    <message>
        <location filename="item_actions_tool_bar.cpp" line="64"/>
        <source>Combine mode</source>
        <translation>Режим сочетания</translation>
    </message>
    <message>
        <location filename="item_actions_tool_bar.cpp" line="68"/>
        <source>Exclude and combine mode</source>
        <translation>Режим исключающего сочетания</translation>
    </message>
    <message>
        <location filename="item_actions_tool_bar.cpp" line="72"/>
        <source>Mark as good combination</source>
        <translation>Пометить, как &quot;хорошая&quot; комбинация</translation>
    </message>
    <message>
        <location filename="item_actions_tool_bar.cpp" line="76"/>
        <source>Mark as bad combination</source>
        <translation>Пометить как плохое сочетание</translation>
    </message>
    <message>
        <location filename="item_actions_tool_bar.cpp" line="80"/>
        <source>Select item</source>
        <translation>Отметить ингридиент</translation>
    </message>
    <message>
        <location filename="item_actions_tool_bar.cpp" line="85"/>
        <source>Mark as bad and set property for all
selected items except main item</source>
        <translation>Пометить как плохое сочетание
и добавить свойство во все 
отмеченные ингридиенты кроме 
основного ингридиетна</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <location filename="mainwindow.cpp" line="278"/>
        <location filename="mainwindow.cpp" line="303"/>
        <source>Life is Feudal Alchemy Tool</source>
        <translation>Life is Feudal Набор юного химика</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="34"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">Правка</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="42"/>
        <source>Window</source>
        <translation>Окно</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="48"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="58"/>
        <source>toolBar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="69"/>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="90"/>
        <source>Reset bad combinations</source>
        <translation>Сбросить плохие сочетания</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="95"/>
        <source>Language Settings</source>
        <translation>Языковые настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="105"/>
        <source>View help</source>
        <translation>Получить помощь</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="110"/>
        <source>Create new</source>
        <translation>Создать новый</translation>
    </message>
    <message>
        <source>LanguageSettings</source>
        <translation type="vanished">НастройкиЯзыка</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="100"/>
        <source>Recalc missed props</source>
        <translation>Пересчитать отсутствующие свойства</translation>
    </message>
    <message>
        <source>Language settings</source>
        <translation type="vanished">Языковые настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="85"/>
        <source>Enable filters</source>
        <translation>Фильтры</translation>
    </message>
    <message>
        <source>set Filters</source>
        <translation type="vanished">Установить фильтры</translation>
    </message>
    <message>
        <source>clear state</source>
        <translation type="vanished">Очистить</translation>
    </message>
    <message>
        <source>make second try</source>
        <translation type="vanished">Пересчитать</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="74"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <source>Load</source>
        <translation type="vanished">Загрузить</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="vanished">Изображение</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Название</translation>
    </message>
    <message>
        <source>Effects</source>
        <translation type="vanished">Эффект</translation>
    </message>
    <message>
        <source>Icon</source>
        <translation type="vanished">Иконка</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="vanished">Свойства</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="59"/>
        <location filename="mainwindow.cpp" line="321"/>
        <source>Filters</source>
        <translation>Фильтры</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="79"/>
        <location filename="mainwindow.cpp" line="322"/>
        <source>Clear bad combinations</source>
        <translation>Очистить плохие комбинации</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="204"/>
        <source>Open file</source>
        <translation>Открыть файл</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="204"/>
        <source>Alchemy files (*.alc)</source>
        <translation>Файлы юного химика (*alc)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="300"/>
        <source>Opening error</source>
        <translation>Ошибка открытия файла</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="300"/>
        <source>Can&apos;t open file</source>
        <translation>Не могу открыть файл, звиняй</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="323"/>
        <location filename="mainwindow.cpp" line="407"/>
        <source>Rescent</source>
        <translation>Последние</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="211"/>
        <source>Save file</source>
        <translation>Сохранить файл</translation>
    </message>
    <message>
        <source>No more properties</source>
        <translation type="vanished">Больше свойств нет</translation>
    </message>
    <message>
        <source>You alredy tried all available properties. Please, try again from beginning</source>
        <translation type="vanished">Вы уже перепробовали все доступные свойства. Попробуйте начать с самого начала.</translation>
    </message>
    <message>
        <source>Flux</source>
        <translation type="vanished">Присадка</translation>
    </message>
    <message>
        <source>Naphta</source>
        <translation type="vanished">Нафта</translation>
    </message>
    <message>
        <source>Food Flavour</source>
        <translation type="vanished">Приправа</translation>
    </message>
    <message>
        <source>Lowers max sHP</source>
        <translation type="vanished">Понижает максимум сознания</translation>
    </message>
    <message>
        <source>Damage sHP</source>
        <translation type="vanished">Наносит урон сознанию</translation>
    </message>
    <message>
        <source>RestoresHP</source>
        <translation type="vanished">Восстанавливает сознание</translation>
    </message>
    <message>
        <source>Damage hHP</source>
        <translation type="vanished">Наносит урон здоровью</translation>
    </message>
    <message>
        <source>Antidote</source>
        <translation type="vanished">Антидот</translation>
    </message>
    <message>
        <source>Damage hStam</source>
        <translation type="vanished">Наносит урон марафонской выносливости</translation>
    </message>
    <message>
        <source>Restore hStam</source>
        <translation type="vanished">Восстанавливает марафонскую выносливость</translation>
    </message>
    <message>
        <source>Damage sStam regen</source>
        <translation type="vanished">Ухудшает восстановление спринтерской выносливости</translation>
    </message>
    <message>
        <source>Lowers max sStam</source>
        <translation type="vanished">Понижает максимум спринтерской выносливости</translation>
    </message>
    <message>
        <source>Raise max sStam</source>
        <translation type="vanished">Повышает максимум спринтерской выносливости</translation>
    </message>
    <message>
        <source>Raise Con</source>
        <translation type="vanished">Повышает телосложение</translation>
    </message>
    <message>
        <source>Raise Will</source>
        <translation type="vanished">Повышает силу воли</translation>
    </message>
    <message>
        <source>Raise Int</source>
        <translation type="vanished">Повышает интеллект</translation>
    </message>
    <message>
        <source>Raise Str</source>
        <translation type="vanished">Повышает силу</translation>
    </message>
    <message>
        <source>Raise Agi</source>
        <translation type="vanished">Повышает ловкость</translation>
    </message>
    <message>
        <source>Alchemy files</source>
        <translation type="vanished">Файлы юного химика</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="218"/>
        <source>Saving error</source>
        <translation>Ошибка сохранения</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="218"/>
        <source>Can&apos;t save file</source>
        <translation>Не могу сохранить твои каракули, звиняй</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="229"/>
        <source>Set language</source>
        <translation>Выберите язык</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="229"/>
        <location filename="mainwindow.cpp" line="231"/>
        <source>English</source>
        <translation>Английский</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="229"/>
        <location filename="mainwindow.cpp" line="233"/>
        <source>Russian</source>
        <translation>Русиш</translation>
    </message>
</context>
<context>
    <name>PropDialog</name>
    <message>
        <location filename="prop_dialog.ui" line="14"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="prop_dialog.cpp" line="10"/>
        <source>Pick properties from list</source>
        <translation>Выберите свойства из списка</translation>
    </message>
</context>
<context>
    <name>PropTranslator</name>
    <message>
        <source>Flux</source>
        <translation type="vanished">Присадка</translation>
    </message>
    <message>
        <source>Naphta</source>
        <translation type="vanished">Нафта</translation>
    </message>
    <message>
        <source>Food Flavour</source>
        <translation type="vanished">Приправа</translation>
    </message>
    <message>
        <source>Lowers max sHP</source>
        <translation type="vanished">Понижает максимум сознания</translation>
    </message>
    <message>
        <source>Damage sHP</source>
        <translation type="vanished">Наносит урон сознанию</translation>
    </message>
    <message>
        <source>RestoresHP</source>
        <translation type="vanished">Восстанавливает сознание</translation>
    </message>
    <message>
        <source>Damage hHP</source>
        <translation type="vanished">Наносит урон здоровью</translation>
    </message>
    <message>
        <source>Antidote</source>
        <translation type="vanished">Антидот</translation>
    </message>
    <message>
        <source>Damage hStam</source>
        <translation type="vanished">Наносит урон марафонской выносливости</translation>
    </message>
    <message>
        <source>Restore hStam</source>
        <translation type="vanished">Восстанавливает марафонскую выносливость</translation>
    </message>
    <message>
        <source>Lowers max sStam</source>
        <translation type="vanished">Понижает максимум спринтерской выносливости</translation>
    </message>
    <message>
        <source>Raise max sStam</source>
        <translation type="vanished">Повышает максимум спринтерской выносливости</translation>
    </message>
    <message>
        <source>Raise Con</source>
        <translation type="vanished">Повышает телосложение</translation>
    </message>
    <message>
        <source>Raise Will</source>
        <translation type="vanished">Повышает силу воли</translation>
    </message>
    <message>
        <source>Raise Int</source>
        <translation type="vanished">Повышает интеллект</translation>
    </message>
    <message>
        <source>Raise Str</source>
        <translation type="vanished">Повышает силу</translation>
    </message>
    <message>
        <source>Raise Agi</source>
        <translation type="vanished">Повышает ловкость</translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <source>Flux</source>
        <translation type="vanished">Присадка</translation>
    </message>
    <message>
        <source>Naphta</source>
        <translation type="vanished">Нафта</translation>
    </message>
    <message>
        <source>Food Flavour</source>
        <translation type="vanished">Приправа</translation>
    </message>
    <message>
        <source>Lowers max sHP</source>
        <translation type="vanished">Понижает максимум сознания</translation>
    </message>
    <message>
        <source>Damage sHP</source>
        <translation type="vanished">Наносит урон сознанию</translation>
    </message>
    <message>
        <source>RestoresHP</source>
        <translation type="vanished">Восстанавливает сознание</translation>
    </message>
    <message>
        <source>Damage hHP</source>
        <translation type="vanished">Наносит урон здоровью</translation>
    </message>
    <message>
        <source>Antidote</source>
        <translation type="vanished">Антидот</translation>
    </message>
    <message>
        <source>Damage hStam</source>
        <translation type="vanished">Наносит урон марафонской выносливости</translation>
    </message>
    <message>
        <source>Restore hStam</source>
        <translation type="vanished">Восстанавливает марафонскую выносливость</translation>
    </message>
    <message>
        <source>Damage sStam regen</source>
        <translation type="vanished">Ухудшает восстановление спринтерской выносливости</translation>
    </message>
    <message>
        <source>Lowers max sStam</source>
        <translation type="vanished">Понижает максимум спринтерской выносливости</translation>
    </message>
    <message>
        <source>Raise max sStam</source>
        <translation type="vanished">Повышает максимум спринтерской выносливости</translation>
    </message>
    <message>
        <source>Raise Con</source>
        <translation type="vanished">Повышает телосложение</translation>
    </message>
    <message>
        <source>Raise Will</source>
        <translation type="vanished">Повышает силу воли</translation>
    </message>
    <message>
        <source>Raise Int</source>
        <translation type="vanished">Повышает интеллект</translation>
    </message>
    <message>
        <source>Raise Str</source>
        <translation type="vanished">Повышает силу</translation>
    </message>
    <message>
        <source>Raise Agi</source>
        <translation type="vanished">Повышает ловкость</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Flux</source>
        <translation type="vanished">Присадка</translation>
    </message>
    <message>
        <source>Naphta</source>
        <translation type="vanished">Нафта</translation>
    </message>
    <message>
        <source>Food Flavour</source>
        <translation type="vanished">Приправа</translation>
    </message>
    <message>
        <source>Lowers max sHP</source>
        <translation type="vanished">Понижает максимум сознания</translation>
    </message>
    <message>
        <source>Damage sHP</source>
        <translation type="vanished">Наносит урон сознанию</translation>
    </message>
    <message>
        <source>RestoresHP</source>
        <translation type="vanished">Восстанавливает сознание</translation>
    </message>
    <message>
        <source>Damage hHP</source>
        <translation type="vanished">Наносит урон здоровью</translation>
    </message>
    <message>
        <source>Antidote</source>
        <translation type="vanished">Антидот</translation>
    </message>
    <message>
        <source>Damage hStam</source>
        <translation type="vanished">Наносит урон марафонской выносливости</translation>
    </message>
    <message>
        <source>Restore hStam</source>
        <translation type="vanished">Восстанавливает марафонскую выносливость</translation>
    </message>
    <message>
        <source>Damage sStam regen</source>
        <translation type="vanished">Ухудшает восстановление спринтерской выносливости</translation>
    </message>
    <message>
        <source>Lowers max sStam</source>
        <translation type="vanished">Понижает максимум спринтерской выносливости</translation>
    </message>
    <message>
        <source>Raise max sStam</source>
        <translation type="vanished">Повышает максимум спринтерской выносливости</translation>
    </message>
    <message>
        <source>Raise Con</source>
        <translation type="vanished">Повышает телосложение</translation>
    </message>
    <message>
        <source>Raise Will</source>
        <translation type="vanished">Повышает силу воли</translation>
    </message>
    <message>
        <source>Raise Int</source>
        <translation type="vanished">Повышает интеллект</translation>
    </message>
    <message>
        <source>Raise Str</source>
        <translation type="vanished">Повышает силу</translation>
    </message>
    <message>
        <source>Raise Agi</source>
        <translation type="vanished">Повышает ловкость</translation>
    </message>
</context>
<context>
    <name>herbConsts</name>
    <message>
        <source>Flux</source>
        <translation type="obsolete">Присадка</translation>
    </message>
    <message>
        <source>Naphta</source>
        <translation type="obsolete">Нафта</translation>
    </message>
    <message>
        <source>Food Flavour</source>
        <translation type="obsolete">Приправа</translation>
    </message>
    <message>
        <source>Lowers max sHP</source>
        <translation type="obsolete">Понижает максимум сознания</translation>
    </message>
    <message>
        <source>Damage sHP</source>
        <translation type="obsolete">Наносит урон сознанию</translation>
    </message>
    <message>
        <source>RestoresHP</source>
        <translation type="obsolete">Восстанавливает сознание</translation>
    </message>
    <message>
        <source>Damage hHP</source>
        <translation type="obsolete">Наносит урон здоровью</translation>
    </message>
    <message>
        <source>Antidote</source>
        <translation type="obsolete">Антидот</translation>
    </message>
    <message>
        <source>Damage hStam</source>
        <translation type="obsolete">Наносит урон марафонской выносливости</translation>
    </message>
    <message>
        <source>Restore hStam</source>
        <translation type="obsolete">Восстанавливает марафонскую выносливость</translation>
    </message>
    <message>
        <source>Damage sStam regen</source>
        <translation type="obsolete">Ухудшает восстановление спринтерской выносливости</translation>
    </message>
    <message>
        <source>Lowers max sStam</source>
        <translation type="obsolete">Понижает максимум спринтерской выносливости</translation>
    </message>
    <message>
        <source>Raise max sStam</source>
        <translation type="obsolete">Повышает максимум спринтерской выносливости</translation>
    </message>
    <message>
        <source>Raise Con</source>
        <translation type="obsolete">Повышает телосложение</translation>
    </message>
    <message>
        <source>Raise Will</source>
        <translation type="obsolete">Повышает силу воли</translation>
    </message>
    <message>
        <source>Raise Int</source>
        <translation type="obsolete">Повышает интеллект</translation>
    </message>
    <message>
        <source>Raise Str</source>
        <translation type="obsolete">Повышает силу</translation>
    </message>
    <message>
        <source>Raise Agi</source>
        <translation type="obsolete">Повышает ловкость</translation>
    </message>
</context>
</TS>
