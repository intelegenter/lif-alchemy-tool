#-------------------------------------------------
#
# Project created by QtCreator 2019-08-31T18:09:13
#
#-------------------------------------------------

RC_ICONS = resources/Alchemy.ico

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LIF_Alchemy
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
  alchemy_table_view.cpp \
  help_widget.cpp \
  herb_proxy_model.cpp \
  herbs.cpp \
  herbs_data_model.cpp \
  item_actions_tool_bar.cpp \
        main.cpp \
        mainwindow.cpp \
    filtersdialog.cpp \
  prop_dialog.cpp \
  props_column_delegate.cpp \
  props_model.cpp \
  propset.cpp

HEADERS += \
  alchemy_table_view.h \
  enums.h \
  help_widget.h \
  herb_proxy_model.h \
  herbs_data_model.h \
  item_actions_tool_bar.h \
        mainwindow.h \
    herbs.h \
    filtersdialog.h \
  prop_dialog.h \
  props_column_delegate.h \
  props_model.h \
  propset.h

FORMS += \
  help_widget.ui \
        mainwindow.ui \
    filtersdialog.ui \
  prop_dialog.ui

RESOURCES += \
    resources.qrc

TRANSLATIONS += ru.ts \
    en.ts
