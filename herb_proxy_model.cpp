#include "herb_proxy_model.h"
#include "herbs_data_model.h"

HerbProxyModel::HerbProxyModel(HerbsDataModel *herbsModel, QObject *parent)
  : QSortFilterProxyModel{parent},
  mHerbsModel(herbsModel)
{
  setSourceModel(mHerbsModel);
  if(!mHerbsModel)
    return;
  connect(mHerbsModel, &HerbsDataModel::modeChanged, this, &HerbProxyModel::onModeChanged);
  connect(mHerbsModel, &HerbsDataModel::dataChanged, this, &HerbProxyModel::onModelDataChanged);
  connect(mHerbsModel, &HerbsDataModel::modelReset, this, &HerbProxyModel::onModelReset);
  connect(mHerbsModel, &HerbsDataModel::modelAboutToBeReset, this, &HerbProxyModel::onModelAboutToReset);
}

void HerbProxyModel::setManualFilters(Props::Set pand, Props::Set por, Props::Set pstrictOr, Props::Set pnot)
{
  mAndFilter = pand;
  mOrFilter = por;
  mStrictOrFilter = pstrictOr;
  mNotFilter = pnot;
  invalidate();
}

Props::Set HerbProxyModel::andFilter() const
{
  return mAndFilter;
}

Props::Set HerbProxyModel::orFilter() const
{
  return mOrFilter;
}

Props::Set HerbProxyModel::strictOrFilter() const
{
  return mStrictOrFilter;
}

Props::Set HerbProxyModel::notFilter() const
{
  return mNotFilter;
}

void HerbProxyModel::onModeChanged(Enums::WorkMode mode)
{
  mBadMap.clear();
  mGoodMap.clear();
  mSelectMap.clear();
  mEmptyOrNotPass = false;
  emit highlightedPropSetChanged(Props::Set());
  if(mode == Enums::excCombMode)
  {
    auto first = mHerbsModel->firstItem();
    auto bc = first.data(Enums::badCombinationsRole).value<BCSet>();
    for(auto& b : bc)
    {
      auto ind = mHerbsModel->index(b, 0);
      mBadMap.insert(ind, ind.data(Enums::propertiesRole).value<Props::Set>());
    }
  }
  rescanFilters();
}
void HerbProxyModel::onModelDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QList<int> &roles)
{
  if(roles.contains(Enums::itemStateRole))
  {
    bool hasChanges = false;
    for(int r = topLeft.row(); r <= bottomRight.row(); r++)
    {
      auto index = mHerbsModel->index(r, 0);
      auto state = index.data(Enums::itemStateRole).value<Enums::ItemState>();
      auto props = index.data(Enums::propertiesRole).value<Props::Set>();
      switch(state)
      {
      case Enums::isGood:
        if(!mGoodMap.contains(index))
        {
          mBadMap.remove(index);
          mSelectMap.remove(index);
          mGoodMap[index] = props;
          hasChanges = true;
        }
        break;
      case Enums::isBad:
        if(mSelectMap.remove(index) || mGoodMap.remove(index))
          hasChanges = true;
        break;
      case Enums::isSelect:
        if(!mSelectMap.contains(index))
        {
          mBadMap.remove(index);
          mGoodMap.remove(index);
          mSelectMap[index] = props;
          hasChanges = true;
        }
        break;
      case Enums::isNone:
      case Enums::isExcMode:
      case Enums::isExcCombMode:
      case Enums::isCombMode:
        hasChanges = mGoodMap.remove(index) || mBadMap.remove(index) || mSelectMap.remove(index);
        break;
      }
    }
    if(hasChanges)
      rescanFilters();
  }
  if(roles.contains(Enums::propertiesRole))
  {
    bool hasChanges = false;
    for(int r = topLeft.row(); r <= bottomRight.row(); r++)
    {
      auto index = mHerbsModel->index(r, 0);
      auto props = index.data(Enums::propertiesRole).value<Props::Set>();
      if(mGoodMap.contains(index))
      {
        mGoodMap[index] = props;
        hasChanges = true;
      }
      if(mBadMap.contains(index))
      {
        mBadMap[index] = props;
        hasChanges = true;
      }
      if(mSelectMap.contains(index))
      {
        mSelectMap[index] = props;
        hasChanges = true;
      }
      if(index.row() == mHerbsModel->firstItem().row())
        hasChanges = true;
    }
    if(hasChanges)
      rescanFilters();
  }
  if(mHerbsModel->mode() == Enums::excCombMode && roles.contains(Enums::badCombinationsRole))
  {
    auto first = mHerbsModel->firstItem();
    if(first.row() >= topLeft.row() && first.row() <= bottomRight.row())
    {
      auto bc = first.data(Enums::badCombinationsRole).value<BCSet>();
      mBadMap.clear();
      for(auto& b : bc)
      {
        auto ind = mHerbsModel->index(b, 0);
        mBadMap.insert(ind, ind.data(Enums::propertiesRole).value<Props::Set>());
      }
      rescanFilters();
    }
  }
}

void HerbProxyModel::onModelReset()
{
  mEmptyOrNotPass = false;
  mBadMap.clear();
  mGoodMap.clear();
  mSelectMap.clear();
  emit highlightedPropSetChanged(Props::Set());
  rescanFilters();
}

void HerbProxyModel::onModelAboutToReset()
{
}

void HerbProxyModel::rescanFilters()
{
  mEmptyOrNotPass = false;
  mAndFilter = Props::Set();
  mOrFilter = Props::Set();
  mStrictOrFilter = Props::Set();
  mNotFilter = Props::Set();
  auto firstItemProps = mHerbsModel->firstItem().data(Enums::propertiesRole).value<Props::Set>();

  switch(mHerbsModel->mode())
  {
  case Enums::excMode:
    mStrictOrFilter = Props::makeFullSet();
    mStrictOrFilter.subtract(firstItemProps);
    mNotFilter = firstItemProps;
    for(auto& p : mSelectMap)
      mStrictOrFilter.subtract(p);
    emit highlightedPropSetChanged(mStrictOrFilter);
    break;
  case Enums::excCombMode:
    mNotFilter = firstItemProps;
    mOrFilter = Props::makeFullSet();
    mOrFilter.subtract(firstItemProps);
    for(auto& p : mBadMap)
      mOrFilter.subtract(p);
    if(mOrFilter.isEmpty())
      mEmptyOrNotPass = true;
    for(auto& p : mSelectMap)
      mNotFilter.unite(p);
//    for(auto p : mGoodMap)
//      mOrFilter.subtract(p.subtract(firstItemProps));
    emit highlightedPropSetChanged(mOrFilter);
    break;
  case Enums::combMode:
    mStrictOrFilter = firstItemProps;
    for(auto& p : mSelectMap)
      mStrictOrFilter.intersect(p);
    emit highlightedPropSetChanged(mStrictOrFilter);
  case Enums::manualMode:
    break;
  }

  invalidate();
}

bool HerbProxyModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
  auto index = mHerbsModel->index(source_row, 0);
  auto props = mHerbsModel->data(index, Enums::propertiesRole).value<Props::Set>();
  auto vstate = mHerbsModel->data(index, Enums::itemStateRole);
  if(vstate.isValid() && vstate.value<Enums::ItemState>() != Enums::isNone)
    return true;

  auto checkAnd = [this, props]()->bool{return mAndFilter.isEmpty() || Props::Set(mAndFilter).subtract(props).isEmpty();};
  auto checkNot = [this, props]()->bool{return mNotFilter.isEmpty() || !mNotFilter.intersects(props);};
  auto checkOr = [this, props](bool emptyOrNotPass = false)->bool{return((!emptyOrNotPass && mOrFilter.isEmpty()) ||
                                                                              props.size() < 3 ||
                                                                         mOrFilter.intersects(props));};
  auto checkStrictOr = [this, props]()->bool{return mStrictOrFilter.isEmpty() || mStrictOrFilter.intersects(props);};

  switch(mHerbsModel->mode())
  {
  case Enums::excMode:
    return checkNot() && checkStrictOr();
  case Enums::excCombMode:
    return checkNot() && checkOr(mEmptyOrNotPass);
  case Enums::combMode:
    return checkStrictOr();
  case Enums::manualMode:
    return checkAnd() && checkNot() && checkOr() && checkStrictOr();
    break;
  }
}
