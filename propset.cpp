#include "propset.h"

Props::Set Props::makeFullSet()
{
  Set propSet;
  for(int i = 0; i < Herb::Properties::LastProperty; i++)
    propSet << (Herb::Properties)i;
  return propSet;
}
