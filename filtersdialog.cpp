#include "filtersdialog.h"
#include "ui_filtersdialog.h"
#include "props_model.h"

#include <QCheckBox>
#include <QPushButton>

FiltersDialog::FiltersDialog(const Props::Set& andF,
                             const Props::Set& orF,
                             const Props::Set& strictOrF,
                             const Props::Set& notF,
                             bool disabled,
                             QWidget *parent) :
  QDialog(parent),
  ui(new Ui::FiltersDialog)
{
  ui->setupUi(this);
  this->setWindowTitle(tr("Pick filters from lists"));
  mModel = new PropsModel(ui->treeView);
  mModel->setColumnCount(1);
  auto fillChildren = [](QStandardItem* item, const Props::Set& checkedProps)
  {
    QSet<Qt::CheckState> statesSet;
    for(int i = 0; i < Herb::Properties::LastProperty; i++)
    {
      auto prop = Herb::Properties(i);
      auto propItem = new QStandardItem(qApp->translate("Constants", Herb::propToStr.at(prop)));
      propItem->setCheckable(true);
      propItem->setAutoTristate(true);
      propItem->setCheckState(checkedProps.contains(prop) ? Qt::Checked : Qt::Unchecked);
      statesSet << propItem->checkState();
      propItem->setData(prop);
      item->appendRow(propItem);
    }
    if(statesSet.size() > 1)
      item->setCheckState(Qt::PartiallyChecked);
    else
      item->setCheckState(*statesSet.begin());
  };
  mAndItem = new QStandardItem(tr("AND"));
  mAndItem->setCheckable(true);
  mAndItem->setAutoTristate(true);
  mOrItem = new QStandardItem(tr("OR"));
  mOrItem->setCheckable(true);
  mOrItem->setAutoTristate(true);
  mStrictOrItem = new QStandardItem(tr("STRICT OR"));
  mStrictOrItem->setCheckable(true);
  mStrictOrItem->setAutoTristate(true);
  mNotItem = new QStandardItem(tr("NOT"));
  mNotItem->setCheckable(true);
  mNotItem->setAutoTristate(true);
  fillChildren(mAndItem, andF);
  fillChildren(mOrItem, orF);
  fillChildren(mStrictOrItem, strictOrF);
  fillChildren(mNotItem, notF);
  mModel->invisibleRootItem()->appendRows({mAndItem, mOrItem, mStrictOrItem, mNotItem});

  ui->treeView->setModel(mModel);

  if(mAndItem->checkState() != Qt::Unchecked)
    ui->treeView->expand(mAndItem->index());
  if(mOrItem->checkState() != Qt::Unchecked)
    ui->treeView->expand(mOrItem->index());
  if(mNotItem->checkState() != Qt::Unchecked)
    ui->treeView->expand(mNotItem->index());
  if(mNotItem->checkState() != Qt::Unchecked)
    ui->treeView->expand(mNotItem->index());
  if(disabled)
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
}

FiltersDialog::~FiltersDialog()
{
  delete ui;
}

Props::Set FiltersDialog::getAndFilters()
{
  return mModel->getPropSet(mAndItem);
}

Props::Set FiltersDialog::getOrFilters()
{
  return mModel->getPropSet(mOrItem);
}

Props::Set FiltersDialog::getStrictOrFilters()
{
  return mModel->getPropSet(mStrictOrItem);
}

Props::Set FiltersDialog::getNotFilters()
{
  return mModel->getPropSet(mNotItem);
}

void FiltersDialog::changeEvent(QEvent *event)
{
  if (event->type() == QEvent::LanguageChange)
  {
    ui->retranslateUi(this);
  }
  QDialog::changeEvent(event);
}
