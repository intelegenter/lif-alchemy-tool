#include "herbs_data_model.h"
#include "herbs.h"
#include <QColor>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QPixmap>

using namespace Enums;

HerbsDataModel::HerbsDataModel(QObject *parent)
  : QAbstractTableModel{parent}
{
  RolesRemapMap iconColumnRemap = {{Qt::DecorationRole, iconRole}};
  RolesRemapMap NameColumnRemap = {{Qt::DisplayRole, nameRole},
                                   {Qt::EditRole, nameRole}};
  RolesRemapMap propsColumnRemap = {{Qt::DisplayRole, propertiesRole},
                                    {Qt::EditRole, propertiesRole}};
  mColumnsRolesRemaps = {{ColumnIcon, iconColumnRemap},
                          {ColumnName, NameColumnRemap},
                          {ColumnDiscoveredProps, propsColumnRemap}};

}

int HerbsDataModel::columnCount(const QModelIndex &parent) const
{
  return ColumnLast;
}

QVariant HerbsDataModel::data(const QModelIndex &ind, int role) const
{
  if(ind.column() < 0 || ind.column() >= ColumnLast)
    return QVariant();
  auto columnRemap = mColumnsRolesRemaps.value((Columns)ind.column());
  if(columnRemap.contains(role))
    role = columnRemap[role];
  switch(role)
  {
  case availableActionsRole:
  {
    if(mFirstItem.isValid() && ind == mFirstItem)
      return Actions(enterExcModeAction | enterCombModeAction | enterExcCombModeAction).toInt();
    switch(mMode)
    {
    case manualMode:
      return Actions(enterExcModeAction | enterCombModeAction | enterExcCombModeAction).toInt();
    case excMode:
      return Actions(selectAction).toInt();
    case combMode:
      return Actions(selectAction).toInt();
    case excCombMode:
      return Actions(goodCombAction | badCombAction | selectAction | badAndSetAction).toInt();
      break;
    }
    break;
  }
  case Qt::BackgroundRole:
  {
    switch(ind.data(Enums::itemStateRole).value<Enums::ItemState>())
    {
    case isNone:
      if(ind.column() == ColumnIcon)
        return QColor(255, 255, 255, 75);
      return QVariant();
    case isGood:
      return QColor(89, 205, 144, 125);
    case isBad:
      return QColor(235, 94, 40, 125);
    case isSelect:
      return QColor(128, 128, 128, 125);
    case isExcMode:
    case isExcCombMode:
    case isCombMode:
      return QColor(241, 162, 8, 125);
    }
    break;
  }
  default:
    return mData.value(ind.row()).value(role);
  }
}

bool HerbsDataModel::setData(const QModelIndex &ind, const QVariant &value, int role)
{
  if(ind.column() < 0 || ind.column() >= ColumnLast)
    return false;
  auto columnRemap = mColumnsRolesRemaps.value((Columns)ind.column());
  if(columnRemap.contains(role))
    role = columnRemap[role];

  int row = ind.row();
  if(mData[row][role] != value)
  {
    if(role == Enums::itemStateRole)
    {
      auto tl = index(row, 0);
      auto br = index(row, ColumnLast - 1);
      if(value.toInt() == Enums::isBad && row != mFirstItem.row())
      {
        auto bc = mData[row].value(Enums::badCombinationsRole, QVariant()).value<BCSet>();
        bc.insert(mFirstItem.row());
        mData[row][Enums::badCombinationsRole] = QVariant::fromValue(bc);
        bc = mData[mFirstItem.row()].value(Enums::badCombinationsRole, QVariant()).value<BCSet>();
        bc.insert(row);
        mData[mFirstItem.row()][Enums::badCombinationsRole] = QVariant::fromValue(bc);
        emit dataChanged(tl, br, {Qt::BackgroundRole, Enums::badCombinationsRole});
        emit dataChanged(mFirstItem, mFirstItem, {Enums::badCombinationsRole});
      }
      else if(row != mFirstItem.row() && mFirstItem.data(Enums::badCombinationsRole).value<BCSet>().contains(row))
      {
        auto bc = mData[row].value(Enums::badCombinationsRole, QVariant()).value<BCSet>();
        bc.remove(mFirstItem.row());
        mData[row][Enums::badCombinationsRole] = QVariant::fromValue(bc);
        bc = mData[mFirstItem.row()].value(Enums::badCombinationsRole, QVariant()).value<BCSet>();
        bc.remove(row);
        mData[mFirstItem.row()][Enums::badCombinationsRole] = QVariant::fromValue(bc);
        emit dataChanged(tl, br, {Qt::BackgroundRole, Enums::badCombinationsRole});
        emit dataChanged(mFirstItem, mFirstItem, {Enums::badCombinationsRole});
      }
      else
      {
        emit dataChanged(tl, br, {Qt::BackgroundRole});
      }
    }
    mData[row][role] = value;
    emit dataChanged(ind, ind, {role});
  }
  return true;
}

bool HerbsDataModel::insertColumns(int column, int count, const QModelIndex &parent)
{
  return false;
}

bool HerbsDataModel::removeColumns(int column, int count, const QModelIndex &parent)
{
  return false;
}

Qt::ItemFlags HerbsDataModel::flags(const QModelIndex &index) const
{
  Qt::ItemFlags flags = Qt::ItemIsEnabled | Qt::ItemIsSelectable;
  if(index.column() == ColumnDiscoveredProps)
    flags |= Qt::ItemIsEditable;
  return flags;
}

void HerbsDataModel::appendRow(QMap<int, QVariant> &item)
{
  beginInsertRows(QModelIndex(), rowCount(), rowCount());
  mData.append(item);
  endInsertRows();
}

void HerbsDataModel::setMode(Enums::WorkMode mode, const QModelIndex &firstItem)
{
  if(mMode == mode)
    return;

  mMode = mode;
  if(rowCount() == 0)
  {
    emit modeChanged(mMode);
    return;
  }

  auto resetItemsState = [this, &firstItem](Enums::ItemState firstItemState = Enums::isNone)
  {
    mFirstItem = firstItem;
    auto bc = firstItem.data(Enums::badCombinationsRole).value<BCSet>();
    for(int r = 0; r < rowCount(); r++)
    {
      auto ind = index(r,0);
      if(r == firstItem.row())
        mData[r][Enums::itemStateRole] = firstItemState == Enums::isNone ? QVariant() : firstItemState;
      else
      {
        if(firstItemState == Enums::isExcCombMode && bc.contains(r))
          mData[r][Enums::itemStateRole] = Enums::isBad;
        else
          mData[r][Enums::itemStateRole] = QVariant();
      }
    }
    emit dataChanged(index(0, 0),
                     index(rowCount() ? rowCount() - 1 : 0, Columns::ColumnLast - 1),
                     {Enums::availableActionsRole, Enums::itemStateRole, Qt::BackgroundRole});
  };
  switch(mMode)
  {
  case manualMode:
    resetItemsState();
    break;
  case excMode:
    resetItemsState(Enums::isExcMode);
    break;
  case combMode:
    resetItemsState(Enums::isCombMode);
    break;
  case excCombMode:
    resetItemsState(Enums::isExcCombMode);
    break;
  }
  emit modeChanged(mMode);
}

void HerbsDataModel::clear()
{
  beginResetModel();
  mData.clear();
  endResetModel();
  setMode(Enums::manualMode);
}

QModelIndex HerbsDataModel::firstItem() const
{
  return mFirstItem;
}

Enums::WorkMode HerbsDataModel::mode() const
{
  return mMode;
}

QModelIndex HerbsDataModel::index(int row, int column, const QModelIndex &parent) const
{
  return createIndex(row, column, &mData[row]);
}

int HerbsDataModel::rowCount(const QModelIndex &parent) const
{
  return mData.size();
}

QList<QModelIndex> HerbsDataModel::getSelectedItems()
{
  QList<QModelIndex> ret;
  for(int r = 0; r < rowCount(); r++)
  {
    if(mData[r].value(Enums::itemStateRole).value<ItemState>() == Enums::isSelect)
      ret << index(r, 0);
  }
  return ret;
}

bool HerbsDataModel::isSelected(const QModelIndex &index)
{
  return mData[index.row()].value(Enums::itemStateRole).value<ItemState>() == Enums::isSelect;
}

QVariant HerbsDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if(orientation == Qt::Horizontal && role == Qt::DisplayRole)
  {
    switch(section)
    {
    case ColumnIcon:
      return tr("Icon");
    case ColumnName:
      return tr("Name");
    case ColumnActions:
      return tr("Actions");
    case ColumnDiscoveredProps:
      return tr("Properties");
    case ColumnLast:
    default:
      break;
    }
  }
  return QAbstractTableModel::headerData(section, orientation, role);
}

void HerbsDataModel::clearBadCombinations()
{
  for(auto& m : mData)
    m[Enums::badCombinationsRole] = QVariant();
  emit dataChanged(index(0, 0), index(mData.size() - 1, ColumnLast - 1), {Enums::badCombinationsRole});
}

void HerbsDataModel::loadData(QJsonDocument doc)
{
  QJsonArray jherbs = doc["herbs"].toArray();
  QMap<int, QMap<int, QVariant>> loadedHerbs;
  for(int i = 0; i < jherbs.size(); i++)
  {
    QJsonObject jherb = jherbs.at(i).toObject();
    Herb::Herbs herb = (Herb::Herbs)jherb["type"].toInt();

    QJsonArray jprops = jherb["props"].toArray();
    Props::Set props;
    for(int j = 0; j < jprops.size(); j++)
      props.insert((Herb::Properties)jprops[j].toInt());
    QJsonArray jbad = jherb["bad"].toArray();
    BCSet badCombs;
    for(int j = 0; j < jbad.size(); j++)
      badCombs.insert(jbad[j].toInt());

    QMap<int, QVariant> herbItem;
    herbItem[iconRole] = QVariant::fromValue(QPixmap(herbConsts::herbToImgPath.at(herb)));
    herbItem[herbEnumValueRole] = herb;
    herbItem[nameRole] = tr(herbConsts::herbToStr.at(herb));
    herbItem[propertiesRole] = QVariant::fromValue(props);
    herbItem[badCombinationsRole] = QVariant::fromValue(badCombs);

    loadedHerbs[herb] = herbItem;
  }
  //restore missed herbs, if it needs
  for(int h = Herb::Acerba_Moretum; h < Herb::HerbLast; h++)
  {
    auto herb = (Herb::Herbs)h;
    if(!loadedHerbs.contains(herb))
    {
      QMap<int, QVariant> herbItem;
      herbItem[iconRole] = QVariant::fromValue(QPixmap(herbConsts::herbToImgPath.at(herb)));
      herbItem[nameRole] = tr(herbConsts::herbToStr.at(herb));
      herbItem[herbEnumValueRole] = herb;
      herbItem[propertiesRole] = QVariant::fromValue(Props::Set());
      herbItem[badCombinationsRole] = QVariant::fromValue(BCSet());

      loadedHerbs[herb] = herbItem;
    }
  }
  beginResetModel();
  mData.clear();
  mData.resize(loadedHerbs.size());

  for(int r = Herb::Acerba_Moretum; r < Herb::HerbLast; r++)
  {
    mData[r] = loadedHerbs[r];
  }
  mMode = Enums::manualMode;
  endResetModel();
  emit modeChanged(mMode);
}

QJsonDocument HerbsDataModel::saveData()
{
  QJsonArray jherbs;
  for(int r = 0; r < rowCount(); r++)
  {
    auto ind = index(r, 0);
    auto type = ind.data(Enums::herbEnumValueRole).value<Herb::Herbs>();
    auto name = ind.data(Enums::nameRole).toString();
    auto props = ind.data(Enums::propertiesRole).value<Props::Set>();
    auto badCombs = ind.data(Enums::badCombinationsRole).value<BCSet>();
    QJsonObject jherb;
    jherb["type"] = type;
    QJsonArray jprops;
    {
      for(auto prop : props)
        jprops.append(prop);
    }
    QJsonArray jbadCombinations;
    {
      for(auto row : badCombs)
        jbadCombinations.append(row);
    }
    jherb["props"] = jprops;
    jherb["bad"] = jbadCombinations;
    jherbs.append(jherb);
  }
  QJsonObject root;
  root["herbs"] = jherbs;
  return QJsonDocument(root);
}
