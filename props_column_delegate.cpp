#include "props_column_delegate.h"
#include <QPainter>
#include <QTextDocument>
#include <QAbstractTextDocumentLayout>

class TDContextHolder : public QObject
{
public:
  TDContextHolder(QObject* parent):QObject(parent){};
  QAbstractTextDocumentLayout::PaintContext ctx;
};

PropsColumnDelegate::PropsColumnDelegate(QObject *parent)
  : QStyledItemDelegate{parent},
  mTextDocument(new QTextDocument(this)),
  mTdContextHolder(new TDContextHolder(this))
{
}

void PropsColumnDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
  painter->save();
  Q_ASSERT(index.isValid());
  QStyleOptionViewItem opt = option;
  initStyleOption(&opt, index);
  mTextDocument->setHtml(opt.text);
  auto rect = opt.rect;
  auto vback = index.data(Qt::BackgroundRole);
  if(vback.isValid())
  {
    painter->save();
    painter->setPen(Qt::NoPen);
    painter->setBrush(vback.value<QColor>());
    painter->drawRect(rect);
    painter->restore();
  }
  painter->translate(rect.topLeft());
  rect.moveTopLeft(QPoint(0,0));
  mTdContextHolder->ctx.clip = rect;
  mTextDocument->documentLayout()->draw( painter, mTdContextHolder->ctx );
  painter->restore();
}

QWidget *PropsColumnDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
  return nullptr;
}

void PropsColumnDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
}

void PropsColumnDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
}

void PropsColumnDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
  QStyledItemDelegate::updateEditorGeometry(editor, option, index);
}

void PropsColumnDelegate::setHighlightedProps(Props::Set props)
{
  mHighlightedProps = props;
}

QString PropsColumnDelegate::displayText(const QVariant &value, const QLocale &locale) const
{
  QString result;
  auto props = value.value<Props::Set>().values();
  std::sort(props.begin(), props.end());
  QString spanHighlightedOpen = "<span style=\"color: #5a4708; background-color: #E0B115; padding: 4px;\">";
  QString spanCommonOpen = "<span style=\"color: #bfbfbf; padding: 4px;\">";
  QString spanClose = "</span>";
  bool firstRow = true;
  for(auto& prop : props)
  {
    if(mHighlightedProps.contains(prop)/* || prop == Herb::Properties::Damage_hHP*/)
    {
      if(firstRow)
        result.append(QString("%2%1%3\n").arg(qApp->translate("Constants", Herb::propToStr.at(prop))).arg(spanHighlightedOpen).arg(spanClose));
      else
        result.append(QString("<br>%2%1%3</br>\n").arg(qApp->translate("Constants", Herb::propToStr.at(prop))).arg(spanHighlightedOpen).arg(spanClose));
    }
    else
    {
      if(firstRow)
        result.append(QString("%2%1%3\n").arg(qApp->translate("Constants", Herb::propToStr.at(prop))).arg(spanCommonOpen).arg(spanClose));
      else
        result.append(QString("<br>%2%1%3</br>\n").arg(qApp->translate("Constants", Herb::propToStr.at(prop))).arg(spanCommonOpen).arg(spanClose));
    }
    firstRow = false;
  }
  return result;
}


QSize PropsColumnDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
  QStyleOptionViewItem opt = option;
  initStyleOption(&opt, index);
  mTextDocument->setHtml(opt.text);
  return mTextDocument->size().toSize();
}
