#ifndef PROPSCOLUMNDELEGATE_H
#define PROPSCOLUMNDELEGATE_H

#include <QStyledItemDelegate>
#include <propset.h>

class QTextDocument;
class TDContextHolder;
class PropsColumnDelegate : public QStyledItemDelegate
{
  Q_OBJECT
public:
  explicit PropsColumnDelegate(QObject *parent = nullptr);

private:
  Props::Set mHighlightedProps;
  QTextDocument* mTextDocument;
  TDContextHolder* mTdContextHolder;

  // QAbstractItemDelegate interface
public:
  void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
  QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
  void setEditorData(QWidget *editor, const QModelIndex &index) const override;
  void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
  void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
  void setHighlightedProps(Props::Set props);

  // QStyledItemDelegate interface
public:
  QString displayText(const QVariant &value, const QLocale &locale) const override;

  // QAbstractItemDelegate interface
public:
  QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const override;
};

#endif // PROPSCOLUMNDELEGATE_H
