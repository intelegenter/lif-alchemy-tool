#include "props_model.h"

PropsModel::PropsModel(QObject *parent) : QStandardItemModel(parent){}

bool PropsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
  auto res = QStandardItemModel::setData(index, value, role);
  if(!res)
    return false;
  if(role == Qt::CheckStateRole)
  {
    auto item = itemFromIndex(index);
    if(item->checkState() != Qt::PartiallyChecked)
    {
      std::function<void(QStandardItem*, Qt::CheckState)> recursiveSetState =
        [&recursiveSetState, this](QStandardItem* item, Qt::CheckState state)
      {
        for(int c = 0; c < item->rowCount(); c++)
        {
          auto child = item->child(c);
          recursiveSetState(child, state);
        }
        item->setCheckState(state);
        emit dataChanged(item->index(), item->index(), {Qt::CheckStateRole});
      };

      for(int c = 0; c < item->rowCount(); c++)
      {
        auto child = item->child(c);
        recursiveSetState(child, item->checkState());
      }
    }

    Qt::CheckState parentCheckState = Qt::Unchecked;
    auto parent = item->parent();
    while(parent)
    {
      QSet<Qt::CheckState> statesSet;
      for(int c = 0; c < parent->rowCount(); c++)
      {
        auto child = parent->child(c);
        statesSet << child->checkState();
      }
      if(statesSet.size() > 1)
        parentCheckState = Qt::PartiallyChecked;
      else
        parentCheckState = *statesSet.begin();
      parent->setCheckState(parentCheckState);
      emit dataChanged(parent->index(), parent->index(), {Qt::CheckStateRole});
      parent = parent->parent();
    }
  }
  //    endResetModel();
  return true;
}

Props::Set PropsModel::getPropSet(QStandardItem *item)
{
  Props::Set ret;
  if(item->parent())//if it is not top level item
    return ret;
  for(int c = 0; c < item->rowCount(); c++)
  {
    auto child = item->child(c);
    if(child->checkState() == Qt::Checked)
      ret << child->data().value<Herb::Properties>();
  }
  return ret;
};
