#ifndef PROPSET_H
#define PROPSET_H
#include "herbs.h"

namespace Props
{
using Set = QSet<Herb::Properties>;
Set makeFullSet();
}

#endif // PROPSET_H
